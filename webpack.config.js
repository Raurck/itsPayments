/// <binding ProjectOpened='Watch - Development' />

'use strict';

//var WebpackNotifierPlugin = require('webpack-notifier');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
    entry: {
        itspayments: [
            './web/client/app.js',
            './web/client/app.config.js',
            './web/client/app.config.routes.js',
            './web/client/common/param.service.js',
            './web/client/common/users.service.js',
            './web/client/common/period.service.js',
            './web/client/common/userparam.service.js',
            './web/client/common/salary.service.js',
            './web/client/common/settings.constant.js',
            './web/client/common/calculationObject.factory.js',
            './web/client/common/prnumber.filter.js',
            './web/client/common/periodObject.factory.js',
            './web/client/common/requestCounter.factory.js',
            './web/client/common/workSpinner.directive.js',
            './web/client/dictonary/userparam.controller.js',
            './web/client/salaryform/salaryform.controller.js',
            './web/client/salaryform/periodform.controller.js',
            './web/client/salaryparams/salaryparams.controller.js',
            './web/client/print/print.controller.js',
        ]
    },

    output: {
        filename: './web/bin/[name].js'
    },
    devServer: {
        contentBase: '.',
        host: 'payments.its66.ru',
        port: 80
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader'}
        ]
    },

    resolveLoader: {
        modules: ["node_modules"],
    },
    plugins: [
        //new WebpackNotifierPlugin()
        //new UglifyJSPlugin()
    ]
};