<?php
/**
 * Created by PhpStorm.
 * User: agkufko
 * Date: 10.08.17
 * Time: 11:19
 */
namespace Project\App\HTTP;

use PHPixie\HTTP\Request;

/**
 * Lists the messages
 */
class Users extends Processor
{
    /**
     * @param Request $request HTTP request
     * @return mixed
     */
    public function defaultAction($request)
    {
        $components = $this->components();

        // Get all the messages
        $users = $components->orm()->query('user')->find()->asArray(true);

        // Render the template
        return $users;
    }
}