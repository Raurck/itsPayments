<?php
/**
 * Created by PhpStorm.
 * User: agkufko
 * Date: 10.08.17
 * Time: 11:30
 */

namespace Project\App\HTTP;

use PHPixie\HTTP\Request;

/**
 * Lists the messages
 */
class PaymentsParams extends Processor
{
    /**
     * @param Request $request HTTP request
     * @return mixed
     */
    public function defaultAction($request)
    {
        $components = $this->components();

        // Get all the messages
        $paymentparams = $components->orm()->query('employee_salary_param')->find()->asArray(true);

        // Render the template
        return $paymentparams;
    }

    protected static function getArrayKeyValueSafe($arrayObj, $keyName, $default = null)
    {
        $val =  array_key_exists($keyName, $arrayObj) ? $arrayObj[$keyName] : $default;
        if($val === true){
            $val = 't';
        }
        if($val === false){
            $val = 'f';
        }

        return $val;
    }

    public function saveAction($request)
    {
        $orm = $this->components()->orm();
        $newParam = $request->data()->get('param');
        if ($this->isPost($request)) {
            $entity = $orm->createEntity('employee_salary_param');
        }
        if ($this->isPut($request)) {
            $entity = $orm->query('employee_salary_param')->where('id', $newParam['id'])->findOne();
        }

        $entity->param = self::getArrayKeyValueSafe($newParam, 'param');
        $entity->relatedField = self::getArrayKeyValueSafe($newParam, 'relatedField');
        $entity->monthlySalary = self::getArrayKeyValueSafe($newParam, 'monthlySalary');
        $entity->active = self::getArrayKeyValueSafe($newParam, 'active', 'f');
        $entity->icon = self::getArrayKeyValueSafe($newParam, 'icon');
        $entity->class = self::getArrayKeyValueSafe($newParam, 'class');
        $entity->save();

        // Get all the messages

        // Render the template
        return ['result' => $entity->asObject()];
    }

}