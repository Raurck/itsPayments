<?php
/**
 * Created by PhpStorm.
 * User: agkufko
 * Date: 10.08.17
 * Time: 13:13
 */

namespace Project\App\HTTP;

use PHPixie\HTTP\Request;

class UserParams extends Processor
{

    public function defaultAction($request)
    {
        $components = $this->components();
        $paymentparamsactive = $components->orm()->query('employee_salary_param_active')->find()->asArray(true);

        // Render the template
        return $paymentparamsactive;
    }

    public function saveallAction($request)
    {
        $orm = $this->components()->orm();

        $data = $request->data()->get('params');
        foreach ($data as $key => $value){
            if($value === null){
                continue;
            }
            foreach ($value as $key1 => $activeValue){
                if($activeValue === null){
                    continue;
                }

                $param =  $orm->query('employee_salary_param_active')->where('person_id',intval($key))->and('param_id',intval($key1))->findOne();
                if($param === null){
                    $param = $orm->createEntity('employee_salary_param_active');
                    $param->param_id = $key1;
                    $param->person_id = $key;
                }


                $param->active = $activeValue;
                $param->save();
            }
        }
        return "Ok";
    }
}