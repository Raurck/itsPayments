<?php
/**
 * Created by PhpStorm.
 * User: agkufko
 * Date: 11.08.17
 * Time: 17:09
 */

namespace Project\App\HTTP;

use PHPixie\HTTP\Request;
use Project\App\Helpers as Helpers;

class Periods extends Processor
{
    public function defaultAction($request)
    {
        $orm = $this->components()->orm();
        $periods = $orm->query('salary_period')->orderBy('periodEnd','desc')->find()->asArray(true);
        foreach ($periods as $period) {
            if (!isset($period->datajson)) {
                unset($period->datajson);
            }
        }
        return $periods;
    }


    public function deletePeriodAction($request)
    {
        $orm = $this->components()->orm();
        $id = $request->data()->get('id');
        $period = $orm->query('salary_period')->where('id', $id)->findOne();
        $period->delete();
        return ['period' => 'deleted'];
    }

    public function getPeriodFullDataAction($request)
    {
        $orm = $this->components()->orm();
        $id = $request->data()->get('id');
        $period = $orm->query('salary_period')->where('id', $id)->findOne();

        return ['period' => $period->asObject()];;
    }

    public function saveAction($request)
    {
        $orm = $this->components()->orm();
        $periodObj = $request->data()->get('periodObj');
        $periodStart = new \DateTime($request->data()->get('periodStart'));
        $periodEnd = new \DateTime($request->data()->get('periodEnd'));
        $id = $request->data()->get('id');
        if ($this->isPost($request)) {
            $entity = $orm->createEntity('salary_period');
        }
        if ($this->isPut($request)) {
            $entity = $orm->query('salary_period')->where('id', $id)->findOne();
        }
        $entity->periodStart = $periodStart->format('Y-m-d H:i:s');
        $entity->periodEnd = $periodEnd->format('Y-m-d H:i:s');
        $entity->datajson = $periodObj;
        $entity->save();

        //$http = $this->builder->components()->http();
        //$httpResponses = $http->responses();
        return ['period' => $entity->asObject()];//$httpResponses->response('data added', $headers = array(), 204);
    }

    public function calendarTypesAction($request)
    {
        $orm = $this->components()->orm();
        $types = $orm->query('calendar_type')->find()->asArray(true);
        return $types;
    }

    public function getLengthAction($request)
    {
        $orm = $this->components();
        $periodStart = new \DateTime($request->data()->get('periodStart'));
        $periodEnd = new \DateTime($request->data()->get('periodEnd'));
        $periodDaysStat = Helpers\Helpers::countWorkDaysInPeriod($periodStart, $periodEnd, $orm);
        return $periodDaysStat;
    }

    public function getUserEventsAction($request)
    {
        $orm = $this->components();
        $periodStart = new \DateTime($request->data()->get('periodStart'));
        $periodEnd = new \DateTime($request->data()->get('periodEnd'));
        $periodEventsStat = Helpers\Helpers::countEventsByTypeInPeriod($periodStart, $periodEnd, $orm);
        return $periodEventsStat;
    }

}