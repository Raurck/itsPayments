<?php

namespace Project\App\HTTP;

use Project\App\AppBuilder;

/**
 * Your base web processor class
 */
abstract class Processor extends \PHPixie\DefaultBundle\HTTP\Processor
{
    /**
     * @var AppBuilder
     */
    protected $builder;

    /**
     * @param AppBuilder $builder
     */
    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    protected function isPost($request){
        return $request->method() === 'POST';
    }

    protected function isPut($request){
        return $request->method() === 'PUT';
    }

    protected function isDelete($request){
        return $request->method() === 'DELETE';
    }

    protected function isGet($request){
        return $request->method() === 'GET';
    }

}