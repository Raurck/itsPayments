<?php
/**
 * Created by PhpStorm.
 * User: agkufko
 * Date: 12.10.17
 * Time: 0:09
 */

namespace Project\App\Helpers;


class Helpers
{
    const queryArray = [
        'getHolidaysCountBetweenDatesQuery'=>'with temp_location as (
  select generate_series(?,?, \'1 day\'::interval)::date )
select count(*) from temp_location where is_holiday(generate_series);',

        'userVigilQuery'             => 'select user_id, 
                                      sum(isHolidayVigil) as holidayVigils, 
                                      sum(isEveningVigil) as eveningVigils,
                                      sum(isWheelVigil) as wheelVigil,  
                                      sum(isEveinigPC) as eveinigPC from
                                      (select user_id, 
                                        case
                                          when (is_holiday(calendar.d) = false and event_id = 8) then 1
                                          else 0
                                        end as isEveningVigil,
                                        case
                                          when (is_holiday(calendar.d)  and event_id = 8) then 1
                                          else 0
                                        end as isHolidayVigil,
                                        case
                                          when (is_holiday(calendar.d)  and event_id = 10) then 1
                                          else 0
                                        end as isWheelVigil,
                                        case
                                          when (event_id = 9) then 1
                                          else 0
                                        end as isEveinigPC
                                      from calendar 
                                      where 
                                      calendar.event_id in (8, 9, 10) AND
                                      d >= ? and d<= ?) as extcalendar
                                    group by user_id',
                        'userHolidayWheelVigilQuery' => 'select user_id, count(*) as holidayVigils from calendar 
                                                                        where is_holiday(calendar.d)  AND
                                    calendar.event_id = 10 AND
                                    d >= ? and d<= ?
                                    group by user_id'];
    /*
      0 | Дежурство                               | text-success | alarm
      1 | Больничный                              | text-danger  | local_hospital
      2 | Отпуск                                  | text-warning | local_bar
      3 | Учёба                                   | text-danger  | next_week
      6 | Командировка                            | text-success | local_mall
      7 | Обход                                   | text-success | replay
      8 | Дежурство, вторая смена                 | text-primary | speaker_phone
     10 | Автомобильное дежурство в выходной день | text-primary | directions_car
      4 | Частичное отсутствие                    | text-warning | star_half
      5 | Опоздание                               | text-danger  | alarm_off
      9 | Вечерняя настройка ПК                   | text-success | computer
     * */
    const meaningEventsList = [1, 2, 3, 6, 4, 5];

    public static function countWorkDaysInPeriod(\DateTimeInterface $periodStart, \DateTimeInterface $periodEnd, $components)
    {
        $holidays = self::getHolidaysInPeriod($periodStart,$periodEnd,$components);
        $totalDays = $periodEnd->diff($periodStart)->days + 1;
        return ['totalDays' => $totalDays,
                'holidays'  => $holidays[0]->count,
                'workdays'  => $totalDays - $holidays[0]->count,
        ];
    }

    public static function countEventsByTypeInPeriod(\DateTimeInterface $periodStart, \DateTimeInterface $periodEnd, $components)
    {
        $periodYearStart = new \DateTime($periodStart->format('Y') . '-01-01');

        $eventList = self::getEventInPeriod($periodStart, $periodEnd, $components);
        $yearEventList = self::getEventInPeriod($periodYearStart, $periodEnd, $components);
        $dutyList = self::getSpecialEventInPeriod($periodStart, $periodEnd, $components);

        $userDataList = [];
        self::fillUsersEventDataTable($userDataList, $dutyList, $yearEventList, $eventList);

        return $userDataList;
    }


    protected static function fillUsersEventDataTable(&$targetTable, $dutyList, $yearEventList, $eventList)
    {
        foreach ($dutyList as $row) {
            if (!isset($targetTable[$row->user_id])) {
                $targetTable[$row->user_id] = new \stdClass();
            }
            $targetTable[$row->user_id]->periodDutyCounts = $row->eveningvigils;
            $targetTable[$row->user_id]->periodHolyDayDutyCounts = $row->holidayvigils;
            $targetTable[$row->user_id]->wheelDutyCount = $row->wheelvigil;
            $targetTable[$row->user_id]->pcMaintenanceCount = $row->eveinigpc;
        }

        foreach ($eventList as $row) {
            if (in_array($row->event_id, self::meaningEventsList)) {
                if (!isset($targetTable[$row->user_id])) {
                    $targetTable[$row->user_id] = new \stdClass();
                }
                if (!isset($targetTable[$row->user_id]->events)) {
                    $targetTable[$row->user_id]->events = [];
                }
                if (!isset($targetTable[$row->user_id]->events[$row->event_id])) {
                    $targetTable[$row->user_id]->events[$row->event_id] = new \stdClass();
                    $targetTable[$row->user_id]->events[$row->event_id]->event_id = $row->event_id;
                }

                $targetTable[$row->user_id]->events[$row->event_id]->thisMonth = $row->count;
            }
        }

        foreach ($yearEventList as $row) {
            if (in_array($row->event_id, self::meaningEventsList)) {
                if (!isset($targetTable[$row->user_id])) {
                    continue;
                }
                if (!isset($targetTable[$row->user_id]->events)) {
                    continue;
                }
                if (isset($targetTable[$row->user_id]->events[$row->event_id])) {
                    $targetTable[$row->user_id]->events[$row->event_id]->thisYear = $row->count;
                }
            }
        }

    }

    protected static function getEventInPeriod(\DateTimeInterface $periodStart, \DateTimeInterface $periodEnd, $components)
    {
        $database = $components->database();
        $connection = $database->get('default');

        return $connection->selectQuery()->table('calendar')
            ->fields(array('user_id', 'event_id', 'count' => $database->sqlExpression('COUNT(1)')))
            ->where('d', '>=', $periodStart->format('Y-m-d'))
            ->andWhere('d', '<=', $periodEnd->format('Y-m-d'))
            ->groupBy($database->sqlExpression('user_id, event_id'))->execute()->asArray(true);
    }

    protected static function getSpecialEventInPeriod(\DateTimeInterface $periodStart, \DateTimeInterface $periodEnd, $components)
    {
        $database = $components->database();
        $connection = $database->get('default');

        return $connection->execute(self::queryArray['userVigilQuery'],
            array($periodStart->format('Y-m-d'), $periodEnd->format('Y-m-d'),))->asArray(true);
    }

    protected static function getHolidaysInPeriod(\DateTimeInterface $periodStart, \DateTimeInterface $periodEnd, $components)
    {
        $database = $components->database();
        $connection = $database->get('default');

        return $connection->execute(self::queryArray['getHolidaysCountBetweenDatesQuery'],
            array($periodStart->format('Y-m-d'), $periodEnd->format('Y-m-d'),))->asArray(true);
    }

}