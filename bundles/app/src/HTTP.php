<?php

namespace Project\App;

class HTTP extends \PHPixie\DefaultBundle\HTTP
{
    protected $classMap = array(
        'greet'          => 'Project\App\HTTP\Greet',
        'users'          => 'Project\App\HTTP\Users',
        'paymentsparams' => 'Project\App\HTTP\PaymentsParams',
        'userparams'     => 'Project\App\HTTP\UserParams',
        'periods'        => 'Project\App\HTTP\Periods',
    );
}