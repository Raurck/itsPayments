<?php
/**
 *
 * Created by PhpStorm.
 * User: agkufko
 * Date: 10.08.17
 * Time: 13:26
 */
return array(
    'models' => array(
        'employee_salary_param_active' => array(
            'table' => 'employee_salary_params_active',
        ),
        'calendar' => array(
            'table' => 'calendar',
        ),


        // you can also define embedded models
        // if you are using MongoDB,
        // more on that later
    )
);