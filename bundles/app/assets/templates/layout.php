<!DOCTYPE html>
<html ng-app="itspayments">
	<head>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all" rel="stylesheet">
        <link href="client/node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <link href="client/site.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet">
		<title>ITS Зарплата</title>
		<style>

			body {
				font-family: "Roboto Condensed", Helvetica, sans-serif;
			}

			h1, h2, p {
				text-align: center;
			}

			h1 {
				color: #3f444a;
				font-size: 60px;
			}

			h2 {
				color: #5c6873;
				font-weight: 300;
				font-size: 32px;
			}

			a {
				color: #3f444a;
			}

			a:hover {
				color: #32c5d2;
			}
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
		</style>
	</head>
	<body>
    <header>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" ui-sref="welcome">ITS</a>
                </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown" ui-sref-active="active">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Справочники <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a ui-sref="dictonary">Параметы сотрудников</a></li>
                            <li><a ui-sref="params">Параметры оплаты</a></li>
                        </ul>
                    </li>
                    <li class="dropdown" ui-sref-active="active">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Зарплата <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a ui-sref="salary">Начисление</a></li>

                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <main>
        <work-spinner>
            <div id="loader" class="modal show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: block; padding-right: 17px; background: rgba(0,0,0,0.7);">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row text-center" style="height: 155px;">
                                <div class="loader">Loading...</div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </work-spinner>

        <ui-view></ui-view>
    </main>
    </body>
    <script src="client/node_modules/jquery/dist/jquery.js"></script>
    <script src="client/node_modules/bootstrap/dist/js/bootstrap.js"></script>
    <script src="client/node_modules/angular/angular.js"></script>
    <script src="client/node_modules/angular-animate/angular-animate.js"></script>
    <script src="client/node_modules/angular-messages/angular-messages.js"></script>
    <script src="client/node_modules/angular-touch/angular-touch.js"></script>
    <script src="client/node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js"></script>
    <script src="client/node_modules/angular-ui-router/release/angular-ui-router.js"></script>
    <script src="bin/itspayments.js"></script>

</html>
