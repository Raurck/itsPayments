/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(2);
__webpack_require__(3);
__webpack_require__(4);
__webpack_require__(5);
__webpack_require__(6);
__webpack_require__(7);
__webpack_require__(8);
__webpack_require__(9);
__webpack_require__(10);
__webpack_require__(11);
__webpack_require__(12);
__webpack_require__(13);
__webpack_require__(14);
__webpack_require__(15);
__webpack_require__(16);
__webpack_require__(17);
__webpack_require__(18);
module.exports = __webpack_require__(19);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments', ['ngMessages', 'ngAnimate', 'ui.router', 'ui.bootstrap']);
})();

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').config(httpProvConfigFn).config(configLogFn).config(['$provide', configureTemplateFactory]);

    httpProvConfigFn.$inject = ['$httpProvider'];
    function httpProvConfigFn($httpProvider) {
        $httpProvider.interceptors.push('requestCounter');
    }

    configLogFn.$inject = ['$logProvider'];
    function configLogFn($logProvider) {
        $logProvider.debugEnabled(false);
    };

    configureTemplateFactory.$inject = ['$provide'];
    function configureTemplateFactory($provide) {
        // Set a suffix outside the decorator function
        var cacheBuster = Date.now().toString();

        function templateFactoryDecorator($delegate) {
            var fromUrl = angular.bind($delegate, $delegate.fromUrl);
            $delegate.fromUrl = function (url, params) {
                if (url !== null && angular.isDefined(url) && angular.isString(url)) {
                    url += url.indexOf('?') === -1 ? '?' : '&';
                    url += 'v=' + cacheBuster;
                }

                return fromUrl(url, params);
            };

            return $delegate;
        }

        $provide.decorator('$templateFactory', ['$delegate', templateFactoryDecorator]);
    }
})();

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').config(configFn);

    configFn.$inject = ['$stateProvider', '$urlRouterProvider'];
    function configFn($stateProvider, $urlRouterProvider) {
        $stateProvider.state('welcome', {
            url: '/',
            templateUrl: 'client/welcome/index.html'
        });
        $stateProvider.state('dictonary', {
            url: '/dictonary',
            templateUrl: 'client/dictonary/user.html'
        });
        $stateProvider.state('salary', {
            url: '/salary',
            templateUrl: 'client/salaryform/salaryform.html'
        });
        $stateProvider.state('params', {
            url: '/params',
            templateUrl: 'client/salaryparams/salaryparams.template.html'
        });
        $stateProvider.state('print', {
            url: '/print',
            templateUrl: 'client/print/print.template.html'
        });
        $urlRouterProvider.otherwise('/');
    }
})();

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    angular.module('itspayments').service('paramService', paramService);

    paramService.$inject = ['$http'];

    function paramService($http) {
        var service = this;
        var inProgress = false;
        service.getParamList = getParamList;
        service.saveParam = saveParam;
        service.paymentParams = [];

        init();

        function init() {
            getParamList();
        }

        function saveParam(param) {
            var promise = void 0;
            if (param.id) {
                promise = $http.put('paymentsparams/save', { param: param });
            } else {
                promise = $http.post('paymentsparams/save', { param: param });
            }
            return promise.then(function (data) {
                var result = data.data.result;
                var tmpArr = service.paymentParams.filter(function (params) {
                    return params.id === result.id;
                });
                if (tmpArr.length > 0) {
                    var tmp = tmpArr.pop();
                    tmp.param = result.param;
                    tmp.relatedField = result.relatedField;
                    tmp.monthlySalary = result.monthlySalary;
                    tmp.active = result.active;
                    tmp.icon = result.icon;
                    tmp.class = result.class;
                } else {
                    service.paymentParams.push(result);
                }
                return result;
            });
        }

        function getParamList(force) {
            if (!inProgress || force) {
                inProgress = $http.get('paymentsparams').then(function (data) {
                    service.paymentParams.splice(0, service.paymentParams.length);
                    data.data.map(function (element) {
                        service.paymentParams.push(element);
                    });
                    return data.data;
                });
            }
            return inProgress;
        }
    }
})();

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    angular.module('itspayments').service('userService', userService);

    userService.$inject = ['$http'];

    function userService($http) {
        var service = this;
        service.userList = [];
        service.getUserList = getUserList;
        var inProgress = false;

        init();

        function init() {
            getUserList();
        }

        function getUserList(force) {
            if (!inProgress || force) {
                inProgress = $http.get('users').then(function (data) {
                    service.userList.splice(0, service.userList.length);
                    data.data.map(function (element) {
                        service.userList.push(element);
                    });
                    return data.data;
                });
            }
            return inProgress;
        }
    }
})();

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').service('periodService', periodService);
    periodService.$inject = ['$http', '$log'];

    function periodService($http, $log) {
        var service = this;
        var inProgress = false;

        service.periods = [];
        service.calendarTypes = [];
        service.eventsInPeriod = [];
        service.periodGeneralInfo = {};

        service.getPeriods = getPeriods;
        service.getPeriodGeneralInfo = getPeriodGeneralInfo;
        service.getCalendarTypes = getCalendarTypes;
        service.getUserEventsInPeriod = getUserEventsInPeriod;
        service.knownPeriod = knownPeriod;
        service.savePeriod = savePeriod;
        service.loadPeriod = loadPeriod;
        service.deletePeriod = deletePeriod;

        function toUTCDate(dateInObj) {
            var dateObj = new Date(dateInObj);
            return dateObj.getFullYear() + '-' + (dateObj.getMonth() + 1 < 10 ? 0 : '') + (dateObj.getMonth() + 1) + '-' + (dateObj.getDate() + 1 < 10 ? 0 : '') + dateObj.getDate() + 'T00:00:00.000Z';
        }

        function loadPeriod(id) {
            return $http.post('periods/getPeriodFullData', { id: id }).then(function (data) {
                var periodObj = JSON.parse(data.data.period.datajson);
                periodObj.id = data.data.period.id;
                $log.debug(new Date().toLocaleString('en-GB'), 'periodService', 'loadPeriod', 'received', periodObj);
                return periodObj;
            });
        }

        function deletePeriod(period) {
            return $http.post('periods/deletePeriod', { id: period.id });
        }

        function savePeriod(period) {
            var data = {
                id: period.id,
                periodStart: toUTCDate(period.periodStart),
                periodEnd: toUTCDate(period.periodEnd),
                periodObj: JSON.stringify(period)
            };
            var savePromice = void 0;
            if (parseInt(data.id) === 0) {
                savePromice = $http.post('periods/save', data).then(function (data) {
                    period.id = data.data.period.id;
                });
            } else {
                savePromice = $http.put('periods/save', data);
            }

            return savePromice.then(function () {
                return getPeriods(true);
            });
        }

        function isSameDate(date1, date2) {}

        function knownPeriod(periodBoundary) {
            if (!periodBoundary) {
                return false;
            }
            var foundPeriod = service.periods.filter(function (period) {
                return toUTCDate(periodBoundary.periodStart) === toUTCDate(period.periodStart) && toUTCDate(periodBoundary.periodEnd) === toUTCDate(period.periodEnd);
            }).pop();
            return foundPeriod ? foundPeriod : false;
        }

        function getCalendarTypes() {
            return $http.post('periods/calendarTypes').then(function (data) {
                service.calendarTypes.splice(0, service.calendarTypes.length);
                data.data.map(function (val) {
                    service.calendarTypes.push(val);
                });
                $log.debug(new Date().toLocaleString('en-GB'), 'periodService', 'getCalendarTypes', 'data received', data);
            });
        }

        function getUserEventsInPeriod(period) {
            return $http.post('periods/getUserEvents', { periodStart: toUTCDate(period.periodStart), periodEnd: toUTCDate(period.periodEnd) }).then(function (data) {
                $log.debug(new Date().toLocaleString('en-GB'), 'periodService', 'getUserEventsInPeriod', 'data received', data);
                service.eventsInPeriod = data.data;
                return data.data;
            });
        }

        function getPeriodGeneralInfo(period) {
            return $http.post('periods/getLength', { periodStart: period.periodStart, periodEnd: toUTCDate(period.periodEnd) }).then(function (data) {
                service.periodGeneralInfo = {};
                service.periodGeneralInfo.workdays = data.data.workdays;
                service.periodGeneralInfo.holidays = data.data.holidays;
                service.periodGeneralInfo.totalDays = data.data.totalDays;
                return service.periodGeneralInfo;
            });
        }

        function getPeriods(force) {
            if (!inProgress || force) {
                inProgress = $http.get('periods').then(function (data) {
                    service.periods.splice(0, service.periods.length);
                    if (Array.isArray(data.data)) {
                        data.data.map(function (periodEntry) {
                            service.periods.push({
                                id: parseInt(periodEntry.id),
                                periodStart: new Date(periodEntry.periodStart + ' +0000'),
                                periodEnd: new Date(periodEntry.periodEnd + ' +0000')
                            });
                        });
                    }
                });
            }
            return inProgress;
        }

        init();

        function init() {
            getPeriods();
        }
    }
})();

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').service('userParamService', userParamService);

    userParamService.$inject = ['$http', '$log'];

    function userParamService($http, $log) {
        var service = this;
        service.userParamsList = [];
        service.getUserParamsList = getUserParamsList;
        service.saveUserParamsList = saveUserParamsList;
        var inProgress = false;

        init();

        function init() {
            getUserParamsList();
        }

        function getUserParamsList(force) {
            if (!inProgress || force) {
                inProgress = $http.get('userparams').then(function (data) {
                    service.userParamsList.splice(0, service.userParamsList.length);
                    data.data.map(function (element) {
                        service.userParamsList[element.person_id] = service.userParamsList[element.person_id] || [];
                        service.userParamsList[element.person_id][element.param_id] = parseInt(element.active);
                    });
                    $log.debug(service.userParamsList);
                    return data.data;
                });
            }
            return inProgress;
        }

        function saveUserParamsList() {
            return $http.post('userparams/saveall', { params: service.userParamsList }).then(function () {
                return getUserParamsList(true);
            });
        }
    }
})();

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').service('salaryService', salaryService);

    salaryService.$inject = ['$log', '$q', '$rootScope', 'periodObjectFactory', 'paramService', 'periodService', 'userParamService', 'userService', 'settingsIts'];

    function salaryService($log, $q, $rootScope, periodObjectFactory, paramService, periodService, userParamService, userService, settingsIts) {
        var service = this;
        service.loadedPeriod = {};
        service.activateCurrentPeriod = activateCurrentPeriod;
        service.loadPeriodByHeader = loadPeriodByHeader;
        service.renewUserList = renewUserList;
        service.savePeriod = savePeriod;
        service.deletePeriod = deletePeriod;

        function loadPeriodByHeader(periodHeader) {
            return periodService.loadPeriod(periodHeader.id).then(function (data) {
                service.loadedPeriod = new periodObjectFactory();
                service.loadedPeriod.assign(data);
                $log.debug(new Date().toLocaleString('en-GB'), 'salaryService', 'loadPeriodByHeader', 'get period', service.loadedPeriod);
                $rootScope.$broadcast('newPeriodData');
                return service.loadedPeriod;
            });
        }

        function renewUserList() {
            if (service.loadedPeriod.info.isLoaded) {
                return $q.all([paramService.getParamList(), periodService.getCalendarTypes(), periodService.getUserEventsInPeriod(service.loadedPeriod), userService.getUserList(), userParamService.getUserParamsList()]).then(function () {
                    return service.loadedPeriod.renewUserList(userService.userList, paramService.paymentParams, userParamService.userParamsList, periodService.eventsInPeriod, periodService.calendarTypes);
                });
            }
            return $q.when(true);
        }

        function savePeriod() {
            return periodService.savePeriod(service.loadedPeriod).then(function (data) {
                service.loadedPeriod.changed(false);
                return data;
            });
        }

        function deletePeriod() {
            return periodService.deletePeriod(service.loadedPeriod);
        }

        function activateCurrentPeriod() {
            var periodBoundary = getCurrentPeriodBoundary();

            var foundedPeriod = periodService.knownPeriod(periodBoundary);
            if (foundedPeriod !== false) {
                return loadPeriodByHeader(foundedPeriod);
            }
            return $q.all([paramService.getParamList(), periodService.getPeriodGeneralInfo(periodBoundary), periodService.getCalendarTypes(), periodService.getUserEventsInPeriod(periodBoundary), userService.getUserList(), userParamService.getUserParamsList()]).then(function () {
                service.loadedPeriod = new periodObjectFactory(periodBoundary, periodService.periodGeneralInfo, userService.userList, paramService.paymentParams, userParamService.userParamsList, periodService.eventsInPeriod, periodService.calendarTypes);
                return service.loadedPeriod;
            });
        }

        function getCurrentPeriodBoundary() {
            var toDay = new Date();
            var periodEnd = void 0;
            var periodStart = void 0;
            toDay.setHours(0, 0, 0, 0);
            if (toDay.getDate() <= settingsIts.barierDate) {
                toDay.setUTCDate(-1);
                periodEnd = new Date(toDay);
                toDay.setUTCDate(0);
                periodStart = new Date(toDay);
            } else {
                toDay.setUTCMonth(toDay.getUTCMonth() + 1);
                toDay.setUTCDate(-1);
                periodEnd = new Date(toDay);
                toDay.setUTCDate(0);
                periodStart = new Date(toDay);
            }
            $log.debug(new Date().toLocaleString('en-GB'), 'salaryService', 'loadPeriodByHeader', 'get period', {
                periodStart: periodStart,
                periodEnd: periodEnd
            });
            return {
                periodStart: periodStart,
                periodEnd: periodEnd
            };
        }
    }
})();

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').constant('settingsIts', {
        cardPaymentDefaultAmount: 7400,
        cardPaymentIfNoSymbolId: 6,
        barierDate: 18
    });
})();

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').factory('calculationObjectFactory', calculationObjectFactory);

    calculationObjectFactory.$inject = ['settingsIts'];

    function calculationObjectFactory(settingsIts) {

        function CalculationObject(userId, symbolList, extendedParam) {
            extendedParam = extendedParam || {};
            symbolList = symbolList || [];
            this.userId = userId;
            this.symbols = {
                prepaymentCalc: [],
                postpaymentCalc: [],
                symbolList: angular.copy(symbolList.filter(function (param) {
                    return param.active;
                })),
                incidentSymbolList: symbolList.filter(function (param) {
                    return !!param.relatedField;
                })
            };
            this.bonuses = [];
            this.extendedParams = angular.copy(extendedParam);
            this.prepaymentCalc = 0;
            this.postpaymentCalc = 0;
            this.prepaymentReal = 0;
            this.postpaymentReal = 0;
            this.cardPaymentAmount = 0;
            this.cardPaymentAmountPlaned = 0;
            this.prepaymentDays = null;
            this.postpaymentDays = null;
        }

        CalculationObject.prototype = {
            addBonus: function addBonus(name, amount, paymentInfo) {
                if (!name || isNaN(amount)) {
                    return false;
                }
                this.bonuses.push({ name: name, amount: amount });
                this.calculatePostpaymentSum(paymentInfo);
            },

            removeBonus: function removeBonus(bonus, paymentInfo) {
                var remIndex = this.bonuses.indexOf(bonus);
                if (remIndex >= 0) {
                    this.bonuses.splice(remIndex, 1);
                }
                this.calculatePostpaymentSum(paymentInfo);
            },

            renewPeriodEvents: function renewPeriodEvents(extendedParam) {
                this.extendedParams = angular.copy(extendedParam);
            },

            renewUserSymbols: function renewUserSymbols(symbolsList) {
                this.symbols.symbolList = angular.copy(symbolsList);
                this.symbols.incidentSymbolListFiltred = undefined;
                this.symbols.symbolListFiltred = undefined;
            },

            calculatePostpaymentSum: function calculatePostpaymentSum(paymentInfo) {
                var symbolSum = calculatePostpaymentSymbolSum(this);
                var bonusSum = calculateBonusSum(this);
                var cardSum = calculateCardSum(this, paymentInfo);
                this.cardPaymentAmount = cardSum;
                this.postpaymentCalc = symbolSum + bonusSum - cardSum;
                this.postpaymentReal = this.postpaymentCalc.toFixed(2);
            },

            calculatePrepaymentSum: function calculatePrepaymentSum() {
                var symbolSum = calculatePrepaymentSymbolSum(this);
                this.prepaymentCalc = symbolSum;
                this.prepaymentReal = this.prepaymentCalc.toFixed(2);
            },

            calculate: function calculate(periodInfo) {
                var _this = this;

                this.symbols.incidentSymbolListFiltred = undefined;
                this.symbols.symbolListFiltred = undefined;
                var workDays = periodInfo.workdays;

                if (!periodInfo.prepaidLock) {
                    var prepaymentDays = getUserPrepaidDaysCount(this, periodInfo);
                    var prepayPart = workDays && workDays >= 0 ? prepaymentDays / workDays : 0;

                    this.symbols.prepaymentCalc.splice(0, this.symbols.prepaymentCalc.length);
                    this.symbols.symbolList.map(function (param) {
                        var monthlySalary = parseFloat(param.monthlySalary);
                        monthlySalary = isNaN(monthlySalary) ? 0 : monthlySalary;

                        _this.symbols.prepaymentCalc.push({
                            id: parseInt(param.id),
                            amount: (!param.relatedField ? prepayPart : 0) * monthlySalary
                        });
                    });
                    this.calculatePrepaymentSum(periodInfo);
                }

                if (!periodInfo.postpaidLock) {
                    var postpaymentDays = getUserPostpaidDaysCount(this, periodInfo);
                    var postpayPart = workDays && workDays >= 0 ? postpaymentDays / workDays : 0;

                    this.symbols.postpaymentCalc.splice(0, this.symbols.postpaymentCalc.length);
                    this.symbols.symbolList.map(function (param) {
                        var monthlySalary = parseFloat(param.monthlySalary);
                        monthlySalary = isNaN(monthlySalary) ? 0 : monthlySalary;

                        _this.symbols.postpaymentCalc.push({
                            id: parseInt(param.id),
                            amount: (!param.relatedField ? postpayPart : _this.getExtendedParamValue(param.relatedField)) * monthlySalary
                        });
                    });
                    this.calculatePostpaymentSum(periodInfo);
                }
            },

            getExtendedParamValue: function getExtendedParamValue(paramName) {
                if (!(this.extendedParams && this.extendedParams[paramName])) {
                    return 0;
                }
                var tmpRes = parseInt(this.extendedParams[paramName]);
                return isNaN(tmpRes) ? 0 : tmpRes;
            },

            haveCalendarEvents: function haveCalendarEvents(paramName) {
                return this.extendedParams && this.extendedParams.events;
            },

            symbolsListFilterd: function symbolsListFilterd() {
                var _this2 = this;

                if (!this.symbols.symbolListFiltred) {
                    this.symbols.symbolListFiltred = this.symbols.symbolList.filter(function (param) {
                        return _this2.getPrepaidBySymbol(param.id) + _this2.getPostpaidBySymbol(param.id) > 0;
                    });
                }
                return this.symbols.symbolListFiltred;
            },

            incidentSymbolListFiltered: function incidentSymbolListFiltered() {
                var _this3 = this;

                if (!this.symbols.incidentSymbolListFiltred) {
                    this.symbols.incidentSymbolListFiltred = this.symbols.incidentSymbolList.filter(function (param) {
                        return _this3.extendedParams[param.relatedField] > 0;
                    });
                }
                return this.symbols.incidentSymbolListFiltred;
            },

            haveCountedParams: function haveCountedParams() {
                var _this4 = this;

                return this.symbols.incidentSymbolList.reduce(function (count, param) {
                    return count + _this4.extendedParams[param.relatedField];
                }, 0) > 0;
            },

            getPrepaidBySymbol: function getPrepaidBySymbol(paramId) {
                var res = this.symbols.prepaymentCalc.filter(function (calcRow) {
                    return calcRow.id === paramId;
                }).pop();
                if (res) {
                    return res.amount;
                }
                return 0;
            },

            getPostpaidBySymbol: function getPostpaidBySymbol(paramId) {
                var res = this.symbols.postpaymentCalc.filter(function (calcRow) {
                    return calcRow.id === paramId;
                }).pop();
                if (res) {
                    return res.amount;
                }
                return 0;
            },

            getUserFullSumCalc: function getUserFullSumCalc() {
                return this.prepaymentCalc + this.postpaymentCalc + parseFloat(this.cardPaymentAmount);
            },

            getUserFullSumReal: function getUserFullSumReal() {
                return saveParseFloat(this.prepaymentReal) + saveParseFloat(this.postpaymentReal) + saveParseFloat(this.cardPaymentAmount);
            },

            getUserCardPayment: function getUserCardPayment(paymentInfo) {
                return calculateCardSum(this, paymentInfo);
            },

            getDelta: function getDelta() {
                return this.getUserFullSumReal() - this.getUserFullSumCalc();
            },

            cardPaymentRequired: function cardPaymentRequired() {
                return this.symbols.postpaymentCalc.filter(function (calcRow) {
                    return calcRow.id === settingsIts.cardPaymentIfNoSymbolId;
                }).length === 0;
            }

        };

        function saveParseFloat(Number) {
            var result = parseFloat(Number);
            return isNaN(result) ? 0 : result;
        }

        function calculatePostpaymentSymbolSum(userCalcObject) {
            return userCalcObject.symbols.postpaymentCalc.reduce(function (sum, value) {
                return sum + value.amount;
            }, 0);
        }

        function calculatePrepaymentSymbolSum(userCalcObject) {
            return userCalcObject.symbols.prepaymentCalc.reduce(function (sum, value) {
                return sum + value.amount;
            }, 0);
        }

        function calculateBonusSum(userCalcObject) {
            return userCalcObject.bonuses.reduce(function (sum, val) {
                return sum + val.amount;
            }, 0);
        }

        function calculateCardSum(userCalcObject, paymentInfo) {
            if (userCalcObject.cardPaymentRequired()) {
                var amount = 0;
                //if (paymentInfo && paymentInfo.cardPaymentAmount) {
                amount = parseFloat(userCalcObject.cardPaymentAmountPlaned || (paymentInfo && paymentInfo.cardPaymentAmount ? paymentInfo.cardPaymentAmount : 0) || 0);
                //}
                //userCalcObject.cardPaymentAmount = parseFloat(userCalcObject.cardPaymentAmount || amount);
                return amount; //userCalcObject.cardPaymentAmount;
            }
            return 0;
        }

        function getUserPrepaidDaysCount(userCalcObject, periodInfo) {
            if (userCalcObject.prepaymentDays) {
                return userCalcObject.prepaymentDays;
            }

            if (userCalcObject.postpaymentDays) {
                return periodInfo.workdays - userCalcObject.postpaymentDays;
            }

            if (periodInfo.prepaymentDays) {
                return periodInfo.prepaymentDays;
            }

            if (periodInfo.postpaymentDays) {
                return periodInfo.workdays - periodInfo.postpaymentDays;
            }

            return Math.floor(parseFloat(periodInfo.workdays) / 2);
        }

        function getUserPostpaidDaysCount(userCalcObject, periodInfo) {
            if (userCalcObject.postpaymentDays) {
                return userCalcObject.postpaymentDays;
            }

            if (userCalcObject.prepaymentDays) {
                return periodInfo.workdays - userCalcObject.prepaymentDays;
            }

            if (periodInfo.postpaymentDays) {
                return periodInfo.postpaymentDays;
            }

            if (periodInfo.prepaymentDays) {
                return periodInfo.workdays - periodInfo.prepaymentDays;
            }

            return Math.ceil(parseFloat(periodInfo.workdays) / 2);
        }

        return CalculationObject;
    }
})();

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').filter('prnumber', function () {
        return function (input) {
            input = parseInt(input || '0', 10);
            //return input;
            if (input === 0) {
                return 0;
            }
            var out = '.00';
            var sign = '';
            if (input < 0) {
                sign = '-';
                input *= -1;
            }
            for (var i = 0; input > 0; i++) {
                out = '' + input % 10 + out;
                if ((i + 1) % 3 === 0) {
                    out = '\xa0' + out;
                }
                input = ~~(input / 10); // Math.floor()
            }

            return '' + sign + out;
        };
    });
})();

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').factory('periodObjectFactory', periodObjectFactory);

    periodObjectFactory.$inject = ['$log', 'settingsIts', 'calculationObjectFactory'];

    function periodObjectFactory($log, settingsIts, calculationObjectFactory) {

        function PeriodObject(periodBoundary, periodGeneralInfo, userList, paymentParams, usersParamsList, periodEventList, eventTypes) {
            userList = userList || [];
            paymentParams = paymentParams || [];
            usersParamsList = usersParamsList || [];
            periodEventList = periodEventList || [];
            eventTypes = eventTypes || [];
            periodBoundary = periodBoundary || {};
            periodGeneralInfo = periodGeneralInfo || {};

            this.id = 0;

            this.periodStart = periodBoundary.periodStart;
            this.periodEnd = periodBoundary.periodEnd;

            this.info = {
                workdays: 0,
                holidays: 1,
                totalDays: 1,
                prepaymentDays: undefined,
                postpaymentDays: undefined,
                cardPaymentAmount: settingsIts.cardPaymentDefaultAmount,
                prepaidLock: false,
                postpaidLock: false,
                isLoaded: false,
                isSaved: false
            };

            setInfo(this, periodGeneralInfo);

            this.params = {
                eventTypes: angular.copy(eventTypes)
            };

            this.userList = this.fillUserList(userList, paymentParams, usersParamsList, periodEventList);
        }

        PeriodObject.prototype = {
            calculate: function calculate() {
                var _this = this;

                if (!(this.postpaidLock && this.prepaidLock)) {
                    this.changed(true);
                }
                this.userList.map(function (user) {
                    return user.calculationObject.calculate(_this.info);
                });
            },

            addBonus: function addBonus(user, name, amount) {
                if (!this.info.isPostpaymentLocked) {
                    this.changed(true);
                    user.calculationObject.addBonus(name, amount, this.info);
                }
            },

            getFullPostpaySum: function getFullPostpaySum() {
                return this.userList.reduce(function (sum, user) {
                    return sum + saveParseFloat(user.calculationObject.postpaymentReal);
                }, 0);
            },

            getFullPrepaySum: function getFullPrepaySum() {
                return this.userList.reduce(function (sum, user) {
                    return sum + saveParseFloat(user.calculationObject.prepaymentReal);
                }, 0);
            },

            getFullCardSum: function getFullCardSum() {
                return this.userList.reduce(function (sum, user) {
                    return sum + saveParseFloat(user.calculationObject.cardPaymentAmount);
                }, 0);
            },

            getFullFOTSum: function getFullFOTSum() {
                return this.userList.reduce(function (sum, user) {
                    return sum + user.calculationObject.getUserFullSumReal();
                }, 0);
            },

            changed: function changed(state) {
                this.info.isSaved = !state;
            },

            removeBonus: function removeBonus(user, bonus) {
                if (!this.info.isPostpaymentLocked) {
                    this.changed(true);
                    user.calculationObject.removeBonus(bonus, this.info);
                }
            },

            renewUserList: function renewUserList(userList, paymentParams, usersParamsList, periodEventList, eventTypes) {
                var _this2 = this;

                if (this.info.isLoaded) {
                    this.userList.map(function (user) {
                        user.calculationObject.renewPeriodEvents(periodEventList[user.id] || []);
                    });

                    this.userList.map(function (user) {
                        user.calculationObject.renewUserSymbols(filterUsersParams(user.id, paymentParams, usersParamsList) || []);
                    });

                    if (!this.info.prepaidLock) {
                        this.params.eventTypes = angular.copy(eventTypes);
                    }
                    if (!this.info.postpaidLock) {
                        var tmpUser = this.fillUserList(userList, paymentParams, usersParamsList, periodEventList);
                        this.userList = this.userList.concat(tmpUser.filter(function (tmpUser) {
                            return _this2.userList.filter(function (user) {
                                return user.id === tmpUser.id;
                            }).length === 0;
                        }));

                        if (this.info.prepaidLock && eventTypes && Array.isArray(eventTypes) && eventTypes.length > 0) {
                            this.params.prepaidEventTypes = angular.copy(this.params.eventTypes);
                            this.params.eventTypes = angular.copy(eventTypes);
                        }
                    }
                }
                $log.debug(new Date().toLocaleString('en-GB'), 'periodObjectFactory', 'renewUserList', 'new data', this);
            },

            fillUserList: function fillUserList(userList, paymentParams, usersParamsList, periodEventList) {
                if (Array.isArray(userList) && Array.isArray(usersParamsList)) {
                    return userList.filter(function (user) {
                        return parseInt(user.state) === 1 && Array.isArray(usersParamsList[parseInt(user.id)]) && usersParamsList[parseInt(user.id)].reduce(function (hasAnyParam, currentParam) {
                            return hasAnyParam || currentParam;
                        }, false);
                    }).map(function (user) {
                        return {
                            id: user.id,
                            fullname: user.fullname,
                            calculationObject: new calculationObjectFactory(user.id, filterUsersParams(user.id, paymentParams, usersParamsList), periodEventList[user.id] || [])
                        };
                    });
                }
                return [];
            },

            getCalendarTypeById: function getCalendarTypeById(typeId) {
                return this.params.eventTypes.filter(function (x) {
                    return x.id === typeId;
                }).pop() || {};
            },

            getUserCardPayment: function getUserCardPayment(user) {
                return user.getUserCardPayment(this.info);
            },

            assign: function assign(dataObj) {
                if (!dataObj) {
                    $log.warn(new Date().toLocaleString('en-GB'), 'periodObjectFactory', 'assign', 'dataObj was empty');
                    return;
                }
                Object.assign(this, dataObj);
                /*this.id = dataObj.id;
                //this.params = angular.copy(dataObj.params);
                //this.periodStart = dataObj.periodStart;
                //this.periodEnd = dataObj.periodEnd;
                //setInfo(this, dataObj.info);
                //this.userList = angular.copy(dataObj.userList);*/
                this.info.isLoaded = true;

                var tmp = new calculationObjectFactory();
                this.userList.map(function (userObj) {
                    var x = Object.create(tmp.__proto__);
                    Object.assign(x, userObj.calculationObject);
                    userObj.calculationObject = x;
                });

                this.changed(false);
                $log.debug(new Date().toLocaleString('en-GB'), 'periodObjectFactory', 'assign', 'dataObj assign result', this);
            }
        };

        function setInfo(periodObj, newInfo) {
            periodObj.info.workdays = newInfo.workdays || periodObj.info.workdays;
            periodObj.info.holidays = newInfo.holidays || periodObj.info.holidays;
            periodObj.info.totalDays = newInfo.totalDays || periodObj.info.totalDays;
            periodObj.info.prepaymentDays = newInfo.prepaymentDays || periodObj.info.prepaymentDays;
            periodObj.info.postpaymentDays = newInfo.postpaymentDays || periodObj.info.postpaymentDays;
            periodObj.info.cardPaymentAmount = newInfo.cardPaymentAmount || periodObj.info.cardPaymentAmount;
            periodObj.info.isPrepaymentLocked = newInfo.isPrepaymentLocked || periodObj.info.isPrepaymentLocked;
            periodObj.info.isPostpaymentLocked = newInfo.isPostpaymentLocked || periodObj.info.isPostpaymentLocked;
        }

        function filterUsersParams(userId, paymentParams, usersParamsList) {
            $log.debug(new Date().toLocaleString('en-GB'), 'periodService', 'filterUsersParams', ' with', arguments, 'start');
            if (Array.isArray(paymentParams) && Array.isArray(usersParamsList) && Array.isArray(usersParamsList[parseInt(userId)])) {
                return paymentParams.filter(function (param) {
                    return usersParamsList[parseInt(userId)].filter(function (currentParamEnabled, idx) {
                        return parseInt(idx) === parseInt(param.id) && currentParamEnabled;
                    }).length > 0;
                });
            }
            return [];
        }

        function saveParseFloat(Number) {
            var result = parseFloat(Number);
            return isNaN(result) ? 0 : result;
        }

        return PeriodObject;
    }
})();

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').factory('requestCounter', requestCounter);

    requestCounter.$inject = ['$q', '$log'];

    function requestCounter($q, $log) {
        var requests = 0;

        function request(config) {
            requests++;
            $log.debug(requests);
            return $q.when(config);
        }

        function requestError(error) {
            requests--;
            $log.debug(requests);
            return $q.reject(error);
        }

        function response(responseInt) {
            requests--;
            $log.debug(requests);
            return $q.when(responseInt);
        }

        function responseError(error) {
            requests--;
            $log.debug(requests);
            return $q.reject(error);
        }
        function getRequestCount() {
            return requests;
        }

        return {
            request: request,
            response: response,
            requestError: requestError,
            responseError: responseError,
            getRequestCount: getRequestCount
        };
    }
})();

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    angular.module('itspayments').directive('workSpinner', workSpinner);

    workSpinner.$inject = ['requestCounter'];

    function workSpinner(requestCounter) {
        return {
            restrict: 'EA',
            transclude: true,
            scope: {},
            template: '<ng-transclude ng-show="requestCount"></ng-transclude>',
            link: function link(scope) {
                scope.$watch(function () {
                    return requestCounter.getRequestCount();
                }, function (requestCount) {
                    scope.requestCount = requestCount;
                });
            }
        };
    }
})();

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').controller('userparmCtrl', userparmCtrl);

    userparmCtrl.$inject = ['$log', 'userService', 'paramService', 'userParamService'];

    function userparmCtrl($log, userService, paramService, userParamService) {
        var param = this;
        param.submitData = submitData;
        param.userlist = userService.userList;
        param.userparams = paramService.paymentParams;
        param.userParamsList = userParamService.userParamsList;

        function submitData() {
            userParamService.saveUserParamsList();
        }
    }
})();

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').controller('salaryformCtrl', salaryformCtrl);

    salaryformCtrl.$inject = ['$log', '$scope', '$q', 'salaryService', 'periodService'];

    function salaryformCtrl($log, $scope, $q, salaryService, periodService) {
        var salaryform = this;
        salaryform.activePeriod = {};

        salaryform.addBonus = addBonus;
        salaryform.isPeriodSet = isPeriodSet;
        salaryform.isCardPaymentRequired = isCardPaymentRequired;
        salaryform.classForAmount = classForAmount;
        salaryform.getCalculation = getCalculation;
        salaryform.getCalendarTypeById = getCalendarTypeById;
        salaryform.getExtendedParamValue = getExtendedParamValue;
        salaryform.getPostpaidBySymbol = getPostpaidBySymbol;
        salaryform.getPrepaidBySymbol = getPrepaidBySymbol;
        salaryform.haveAdditionalData = haveAdditionalData;
        salaryform.haveCountedParams = haveCountedParams;
        salaryform.removeBonus = removeBonus;
        salaryform.getFullPostpaySum = getFullPostpaySum;
        salaryform.getFullPrepaySum = getFullPrepaySum;
        salaryform.getFullFOTSum = getFullFOTSum;
        salaryform.getFullCardSum = getFullCardSum;
        salaryform.getStatusText = getStatusText;
        salaryform.getStatusState = getStatusState;
        salaryform.getSaveText = getSaveText;
        salaryform.getSaveState = getSaveState;
        salaryform.change = onChange;
        salaryform.savePeriod = savePeriod;
        salaryform.deletePeriod = deletePeriod;

        init();

        function init() {
            $log.debug(new Date().toLocaleString('en-GB'), 'salaryformCtrl', 'init', 'start');
            periodService.getPeriods().then(function () {
                if (salaryService.loadedPeriod.id !== undefined) {
                    salaryform.activePeriod = salaryService.loadedPeriod;
                    return $q.when(salaryform.activePeriod);
                }
                return setCurrentPeriod();
            });
            $scope.$on('newPeriodData', function () {
                salaryform.activePeriod = salaryService.loadedPeriod;
            });
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        }

        function setCurrentPeriod() {
            return salaryService.activateCurrentPeriod().then(function (activePeriod) {
                $log.debug(new Date().toLocaleString('en-GB'), 'salaryformCtrl', 'init', 'end');
                salaryform.activePeriod = activePeriod;
            });
        }

        function addBonus(userObj) {
            var name = salaryform.editBonusName[userObj.id];
            var amount = parseFloat(salaryform.editSum[userObj.id]);

            if (!name || isNaN(amount)) {
                return;
            }
            salaryform.activePeriod.addBonus(userObj, name, amount);

            salaryform.editBonusName[userObj.id] = undefined;
            salaryform.editSum[userObj.id] = undefined;
            angular.element('#editBonusName_' + userObj.id).focus();
        }

        function removeBonus(userObj, bonusToRemove) {
            salaryform.activePeriod.removeBonus(userObj, bonusToRemove);
            salaryform.editBonusName[userObj.id] = bonusToRemove.name;
            salaryform.editSum[userObj.id] = bonusToRemove.amount;
            angular.element('#editBonusName_' + userObj.id).focus();
        }

        function classForAmount(amount) {
            return amount >= 0 ? 'text-success' : 'text-danger';
        }

        function getCalculation() {
            return salaryService.renewUserList().then(function () {
                return salaryform.activePeriod.calculate();
            });
        }

        function getCalendarTypeById(typeId) {
            return salaryform.activePeriod.getCalendarTypeById(typeId);
        }

        function getFullPostpaySum() {
            return salaryform.activePeriod.getFullPostpaySum && salaryform.activePeriod.getFullPostpaySum();
        }

        function getFullPrepaySum() {
            return salaryform.activePeriod.getFullPrepaySum && salaryform.activePeriod.getFullPrepaySum();
        }

        function getFullCardSum() {
            return salaryform.activePeriod.getFullCardSum && salaryform.activePeriod.getFullCardSum();
        }

        function getFullFOTSum() {
            return salaryform.activePeriod.getFullFOTSum && salaryform.activePeriod.getFullFOTSum();
        }

        function getStatusText() {
            return getStatusState() ? 'Период загружен из БД' : 'Новый период';
        }

        function getStatusState() {
            return salaryform.activePeriod && salaryform.activePeriod.info && salaryform.activePeriod.info.isLoaded;
        }

        function getSaveState() {
            return salaryform.activePeriod && salaryform.activePeriod.info && salaryform.activePeriod.info.isSaved;
        }

        function getSaveText() {
            return getSaveState() ? 'Изменения сохранены' : 'ВНИМАНИЕ ЕСТЬ НЕ СОХРАНЕННЫЕ ИЗМЕНЕНИЯ';
        }

        function getPostpaidBySymbol(calcObj, paramId) {
            return calcObj.getPostpaidBySymbol(paramId);
        }

        function getPrepaidBySymbol(calcObj, paramId) {
            return calcObj.getPrepaidBySymbol(paramId);
        }

        function getExtendedParamValue(userObj, relatedField) {
            return userObj.calculationObject.getExtendedParamValue(relatedField);
        }

        function haveCountedParams(userObj) {
            return userObj.calculationObject.haveCountedParams();
        }

        function haveAdditionalData(calcObj) {
            return calcObj.haveCalendarEvents();
        }

        function isPeriodSet() {
            return Number.isInteger(salaryform.activePeriod.id);
        }

        function isCardPaymentRequired(calcObj) {
            return calcObj.cardPaymentRequired();
        }

        function onChange() {
            salaryform.activePeriod && salaryform.activePeriod.changed && salaryform.activePeriod.changed(true);
        }

        function savePeriod() {
            return salaryService.savePeriod();
        }

        function deletePeriod() {
            return salaryService.deletePeriod().then(function () {
                return periodService.getPeriods(true);
            }).then(function () {
                return setCurrentPeriod();
            });
        }
    }
})();

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').controller('periodformCtr', periodformCtr);

    periodformCtr.$inject = ['$log', 'periodService', 'salaryService'];

    function periodformCtr($log, periodService, salaryService) {
        var periodform = this;
        periodform.setActive = setActive;
        periodform.setCurrent = setCurrent;
        periodform.isActive = isActive;
        periodform.periodList = [];
        init();

        function init() {
            periodService.getPeriods().then(function () {
                return periodform.periodList = periodService.periods;
            });
        }

        function setActive(period) {
            salaryService.loadPeriodByHeader(period);
        }

        function isActive(period) {
            return period.id == salaryService.loadedPeriod.id;
        }

        function setCurrent() {
            salaryService.activateCurrentPeriod();
        }
    }
})();

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').controller('salaryParamCtrl', salaryParamCtrl);

    salaryParamCtrl.$inject = ['$log', 'paramService'];

    function salaryParamCtrl($log, paramService) {
        var salaryParam = this;
        salaryParam.hasChanges = hasChanges;
        salaryParam.submitData = submitData;
        salaryParam.editData = editData;
        salaryParam.newRow = {};
        salaryParam.paymentParams = [];
        paramService.getParamList().then(function () {
            return salaryParam.paymentParams = angular.copy(paramService.paymentParams);
        });

        function hasChanges(currentRow) {
            if (currentRow.id === undefined) {
                return currentRow.param !== undefined && currentRow.monthlySalary !== undefined;
            }

            var defParam = paramService.paymentParams.find(function (param) {
                return param.id == currentRow.id;
            });
            var dpa = defParam.active === true || defParam.active === 't' ? true : false;
            var cpa = currentRow.active === true || currentRow.active === 't' ? true : false;

            return !(defParam.param === currentRow.param && defParam.relatedField === currentRow.relatedField && defParam.monthlySalary === currentRow.monthlySalary && defParam.class === currentRow.class && defParam.icon === currentRow.icon && dpa === cpa);
        }

        function editData(row) {
            paramService.saveParam(row);
        }

        function submitData(row) {
            paramService.saveParam(row).then(function (result) {
                salaryParam.paymentParams[salaryParam.paymentParams.indexOf(row)].id = result.id;
            });
            salaryParam.paymentParams.push(row);
            salaryParam.newRow = {};
        }
    }
})();

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('itspayments').controller('printCtrl', printCtrl);

    printCtrl.$inject = ['$state', 'salaryService'];

    function printCtrl($state, salaryService) {
        var printData = this;
        printData.clearedParams = undefined;
        printData.getReportDate = getReportDate;
        printData.getSumBySymbol = getSumBySymbol;
        printData.getExtendedParamValue = getExtendedParamValue;
        printData.showAdditionalInfo = showAdditionalInfo;
        printData.getPreparedSum = getPreparedSum;
        printData.getBonusSum = getBonusSum;
        init();

        function init() {
            printData.activePeriod = salaryService.loadedPeriod;
            if (!printData.activePeriod.id) {
                $state.go('salary');
            }
        }

        function getReportDate() {
            var date = new Date(printData.activePeriod.periodStart);
            var month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

            return month[date.getMonth()] + ' ' + date.getFullYear();
        }

        function getSumBySymbol(calcObj, paramId) {
            return calcObj.getPrepaidBySymbol(paramId) + calcObj.getPostpaidBySymbol(paramId);
        }

        function getExtendedParamValue(userObj, relatedField) {
            var res = userObj.calculationObject.getExtendedParamValue(relatedField);
            return res === 0 ? '' : res;
        }

        function getBonusSum(userObj) {
            return userObj.calculationObject.bonuses.reduce(function (c, bonus) {
                return c + bonus.amount;
            }, 0);
        }

        function getPreparedSum(textSum) {
            var result = parseFloat(textSum);
            return isNaN(result) ? 0.00 : result.toFixed(2);
        }

        function showAdditionalInfo(userObj) {
            return userObj.calculationObject.bonuses.length > 0;
        }
    }
})();

/***/ })
/******/ ]);