(function () {
    'use strict';
    angular.module('itspayments')
        .config(configFn);

    configFn.$inject=['$stateProvider','$urlRouterProvider'];
    function configFn ($stateProvider,$urlRouterProvider) {
        $stateProvider.state('welcome',{
            url:'/',
            templateUrl:'client/welcome/index.html'
        });
        $stateProvider.state('dictonary',{
            url:'/dictonary',
            templateUrl:'client/dictonary/user.html'
        });
        $stateProvider.state('salary',{
            url:'/salary',
            templateUrl:'client/salaryform/salaryform.html'
        });
        $stateProvider.state('params',{
            url:'/params',
            templateUrl:'client/salaryparams/salaryparams.template.html'
        });
        $stateProvider.state('print',{
            url:'/print',
            templateUrl:'client/print/print.template.html'
        });
        $urlRouterProvider.otherwise('/');
    }

})();