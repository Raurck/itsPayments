(function () {
    'use strict';

    angular.module('itspayments')
        .controller('printCtrl', printCtrl);

    printCtrl.$inject = ['$state', 'salaryService'];

    function printCtrl($state, salaryService) {
        const printData = this;
        printData.clearedParams = undefined;
        printData.getReportDate = getReportDate;
        printData.getSumBySymbol = getSumBySymbol;
        printData.getExtendedParamValue = getExtendedParamValue;
        printData.showAdditionalInfo = showAdditionalInfo;
        printData.getPreparedSum = getPreparedSum;
        printData.getBonusSum = getBonusSum;
        init();

        function init() {
            printData.activePeriod = salaryService.loadedPeriod;
            if (!printData.activePeriod.id) {
                $state.go('salary');
            }
        }

        function getReportDate() {
            const date = new Date(printData.activePeriod.periodStart);
            const month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

            return `${month[date.getMonth()]} ${date.getFullYear()}`
        }

        function getSumBySymbol(calcObj, paramId) {
            return calcObj.getPrepaidBySymbol(paramId) + calcObj.getPostpaidBySymbol(paramId);
        }

        function getExtendedParamValue(userObj, relatedField) {
            const res = userObj.calculationObject.getExtendedParamValue(relatedField);
            return (res === 0) ? '' : res;
        }

        function getBonusSum(userObj) {
            return userObj.calculationObject.bonuses.reduce((c, bonus) => c + bonus.amount, 0);
        }

        function getPreparedSum(textSum) {
            const result = parseFloat(textSum);
            return isNaN(result) ? 0.00 : result.toFixed(2);
        }

        function showAdditionalInfo(userObj) {
            return userObj.calculationObject.bonuses.length > 0;
        }

    }


})();