(function () {
    'use strict';
    angular.module('itspayments')
        .controller('salaryParamCtrl', salaryParamCtrl);

    salaryParamCtrl.$inject = ['$log', 'paramService'];

    function salaryParamCtrl($log, paramService) {
        const salaryParam = this;
        salaryParam.hasChanges = hasChanges;
        salaryParam.submitData = submitData;
        salaryParam.editData = editData;
        salaryParam.newRow = {};
        salaryParam.paymentParams = [];
        paramService.getParamList().then(() => salaryParam.paymentParams = angular.copy(paramService.paymentParams));

        function hasChanges(currentRow) {
            if (currentRow.id === undefined) {
                return currentRow.param !== undefined && currentRow.monthlySalary !== undefined;
            }

            const defParam = paramService.paymentParams.find(param => param.id == currentRow.id);
            const dpa = (defParam.active === true || defParam.active === 't') ? true : false;
            const cpa = (currentRow.active === true || currentRow.active === 't') ? true : false;


            return !((defParam.param === currentRow.param)
                && (defParam.relatedField === currentRow.relatedField)
                && (defParam.monthlySalary === currentRow.monthlySalary)
                && (defParam.class === currentRow.class)
                && (defParam.icon === currentRow.icon)
                && (dpa === cpa)
            );
        }

        function editData(row) {
            paramService.saveParam(row);
        }

        function submitData(row) {
            paramService.saveParam(row).then((result) => {
                salaryParam.paymentParams[salaryParam.paymentParams.indexOf(row)].id = result.id;
            });
            salaryParam.paymentParams.push(row);
            salaryParam.newRow = {};
        }
    }
})();