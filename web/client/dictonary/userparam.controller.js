(function () {
    'use strict';

    angular.module('itspayments')
        .controller('userparmCtrl',userparmCtrl);

    userparmCtrl.$inject=['$log','userService','paramService','userParamService'];

    function userparmCtrl ($log, userService, paramService,userParamService) {
        const  param = this;
        param.submitData = submitData;
        param.userlist=userService.userList;
        param.userparams=paramService.paymentParams;
        param.userParamsList=userParamService.userParamsList;

        function submitData(){
            userParamService.saveUserParamsList();
        }

    }

})();