(function () {
    'use strict';
    angular.module('itspayments')
        .config(httpProvConfigFn)
        .config(configLogFn)
        .config(['$provide', configureTemplateFactory]);

    httpProvConfigFn.$inject = ['$httpProvider'];
    function httpProvConfigFn($httpProvider) {
        $httpProvider.interceptors.push('requestCounter');
    }




    configLogFn.$inject = ['$logProvider'];
    function configLogFn($logProvider) {
        $logProvider.debugEnabled(false);
    };

    configureTemplateFactory.$inject = ['$provide'];
    function configureTemplateFactory($provide) {
        // Set a suffix outside the decorator function
        var cacheBuster = Date.now().toString();

        function templateFactoryDecorator($delegate) {
            var fromUrl = angular.bind($delegate, $delegate.fromUrl);
            $delegate.fromUrl = function (url, params) {
                if (url !== null && angular.isDefined(url) && angular.isString(url)) {
                    url += (url.indexOf('?') === -1 ? '?' : '&');
                    url += 'v=' + cacheBuster;
                }

                return fromUrl(url, params);
            };

            return $delegate;
        }

        $provide.decorator('$templateFactory', ['$delegate', templateFactoryDecorator]);
    }



})();