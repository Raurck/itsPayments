(function () {
    'use strict';
    angular.module('itspayments')
        .controller('periodformCtr', periodformCtr);

    periodformCtr.$inject = ['$log', 'periodService', 'salaryService'];

    function periodformCtr($log, periodService, salaryService) {
        const periodform = this;
        periodform.setActive = setActive;
        periodform.setCurrent = setCurrent;
        periodform.isActive = isActive;
        periodform.periodList = [];
        init();

        function init() {
            periodService.getPeriods()
                .then(() => periodform.periodList = periodService.periods);
        }

        function setActive(period) {
            salaryService.loadPeriodByHeader(period);

        }

        function isActive(period) {
            return period.id == salaryService.loadedPeriod.id;

        }

        function setCurrent() {
            salaryService.activateCurrentPeriod();
        }

    }
})();