(function () {
    'use strict';
    angular.module('itspayments')
        .controller('salaryformCtrl', salaryformCtrl);

    salaryformCtrl.$inject = ['$log', '$scope', '$q','salaryService', 'periodService'];

    function salaryformCtrl($log, $scope, $q, salaryService, periodService) {
        const salaryform = this;
        salaryform.activePeriod = {};

        salaryform.addBonus = addBonus;
        salaryform.isPeriodSet = isPeriodSet;
        salaryform.isCardPaymentRequired = isCardPaymentRequired;
        salaryform.classForAmount = classForAmount;
        salaryform.getCalculation = getCalculation;
        salaryform.getCalendarTypeById = getCalendarTypeById;
        salaryform.getExtendedParamValue = getExtendedParamValue;
        salaryform.getPostpaidBySymbol = getPostpaidBySymbol;
        salaryform.getPrepaidBySymbol = getPrepaidBySymbol;
        salaryform.haveAdditionalData = haveAdditionalData;
        salaryform.haveCountedParams = haveCountedParams;
        salaryform.removeBonus = removeBonus;
        salaryform.getFullPostpaySum = getFullPostpaySum;
        salaryform.getFullPrepaySum = getFullPrepaySum;
        salaryform.getFullFOTSum = getFullFOTSum;
        salaryform.getFullCardSum = getFullCardSum;
        salaryform.getStatusText = getStatusText;
        salaryform.getStatusState = getStatusState;
        salaryform.getSaveText = getSaveText;
        salaryform.getSaveState = getSaveState;
        salaryform.change = onChange;
        salaryform.savePeriod = savePeriod;
        salaryform.deletePeriod = deletePeriod;

        init();

        function init() {
            $log.debug((new Date()).toLocaleString('en-GB'), 'salaryformCtrl', 'init', 'start');
            periodService.getPeriods().then(() => {
                if (salaryService.loadedPeriod.id !== undefined) {
                    salaryform.activePeriod = salaryService.loadedPeriod;
                    return $q.when(salaryform.activePeriod);
                }
                return setCurrentPeriod();
            });
            $scope.$on('newPeriodData', () => {
                salaryform.activePeriod = salaryService.loadedPeriod;
            })
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        }

        function setCurrentPeriod(){
            return salaryService.activateCurrentPeriod()
                .then((activePeriod => {
                    $log.debug((new Date()).toLocaleString('en-GB'), 'salaryformCtrl', 'init', 'end');
                    salaryform.activePeriod = activePeriod
                }))
        }

        function addBonus(userObj) {
            const name = salaryform.editBonusName[userObj.id];
            const amount = parseFloat(salaryform.editSum[userObj.id]);

            if (!name || isNaN(amount)) {
                return;
            }
            salaryform.activePeriod.addBonus(userObj, name, amount);

            salaryform.editBonusName[userObj.id] = undefined;
            salaryform.editSum[userObj.id] = undefined;
            angular.element(`#editBonusName_${userObj.id}`).focus();

        }

        function removeBonus(userObj, bonusToRemove) {
            salaryform.activePeriod.removeBonus(userObj, bonusToRemove);
            salaryform.editBonusName[userObj.id] = bonusToRemove.name;
            salaryform.editSum[userObj.id] = bonusToRemove.amount;
            angular.element(`#editBonusName_${userObj.id}`).focus();

        }

        function classForAmount(amount) {
            return (amount >= 0 ? 'text-success' : 'text-danger');
        }

        function getCalculation() {
            return salaryService.renewUserList().then(() => salaryform.activePeriod.calculate());
        }

        function getCalendarTypeById(typeId) {
            return salaryform.activePeriod.getCalendarTypeById(typeId);
        }

        function getFullPostpaySum() {
            return salaryform.activePeriod.getFullPostpaySum && salaryform.activePeriod.getFullPostpaySum();
        }

        function getFullPrepaySum() {
            return salaryform.activePeriod.getFullPrepaySum && salaryform.activePeriod.getFullPrepaySum();
        }

        function getFullCardSum() {
            return salaryform.activePeriod.getFullCardSum && salaryform.activePeriod.getFullCardSum();
        }

        function getFullFOTSum() {
            return salaryform.activePeriod.getFullFOTSum && salaryform.activePeriod.getFullFOTSum();
        }

        function getStatusText() {
            return getStatusState() ? 'Период загружен из БД' : 'Новый период';
        }

        function getStatusState() {
            return salaryform.activePeriod && salaryform.activePeriod.info && salaryform.activePeriod.info.isLoaded;
        }

        function getSaveState() {
            return salaryform.activePeriod && salaryform.activePeriod.info && salaryform.activePeriod.info.isSaved;
        }

        function getSaveText() {
            return getSaveState() ? 'Изменения сохранены' : 'ВНИМАНИЕ ЕСТЬ НЕ СОХРАНЕННЫЕ ИЗМЕНЕНИЯ';
        }

        function getPostpaidBySymbol(calcObj, paramId) {
            return calcObj.getPostpaidBySymbol(paramId);
        }

        function getPrepaidBySymbol(calcObj, paramId) {
            return calcObj.getPrepaidBySymbol(paramId);
        }

        function getExtendedParamValue(userObj, relatedField) {
            return userObj.calculationObject.getExtendedParamValue(relatedField)
        }

        function haveCountedParams(userObj) {
            return userObj.calculationObject.haveCountedParams();
        }

        function haveAdditionalData(calcObj) {
            return calcObj.haveCalendarEvents();
        }

        function isPeriodSet() {
            return Number.isInteger(salaryform.activePeriod.id);
        }

        function isCardPaymentRequired(calcObj) {
            return calcObj.cardPaymentRequired();
        }

        function onChange() {
            salaryform.activePeriod && salaryform.activePeriod.changed && salaryform.activePeriod.changed(true);
        }

        function savePeriod() {
            return salaryService.savePeriod();
        }

        function deletePeriod(){
            return salaryService.deletePeriod().then(()=>periodService.getPeriods(true)).then(()=>setCurrentPeriod());
        }

    }
})();