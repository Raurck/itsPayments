(function () {
    'use strict';

    angular.module('itspayments')
        .service('salaryService', salaryService);

    salaryService.$inject = ['$log', '$q', '$rootScope', 'periodObjectFactory', 'paramService',
        'periodService', 'userParamService', 'userService', 'settingsIts'];

    function salaryService($log, $q, $rootScope, periodObjectFactory, paramService,
                           periodService, userParamService, userService, settingsIts) {
        const service = this;
        service.loadedPeriod = {};
        service.activateCurrentPeriod = activateCurrentPeriod;
        service.loadPeriodByHeader = loadPeriodByHeader;
        service.renewUserList = renewUserList;
        service.savePeriod = savePeriod;
        service.deletePeriod = deletePeriod;

        function loadPeriodByHeader(periodHeader) {
            return periodService.loadPeriod(periodHeader.id)
                .then((data) => {
                    service.loadedPeriod = new periodObjectFactory();
                    service.loadedPeriod.assign(data);
                    $log.debug((new Date()).toLocaleString('en-GB'), 'salaryService', 'loadPeriodByHeader', 'get period', service.loadedPeriod);
                    $rootScope.$broadcast('newPeriodData');
                    return service.loadedPeriod;
                });
        }

        function renewUserList() {
            if (service.loadedPeriod.info.isLoaded) {
                return $q.all([paramService.getParamList(),
                    periodService.getCalendarTypes(),
                    periodService.getUserEventsInPeriod(service.loadedPeriod),
                    userService.getUserList(),
                    userParamService.getUserParamsList()
                ]).then(() => service.loadedPeriod.renewUserList(
                    userService.userList,
                    paramService.paymentParams,
                    userParamService.userParamsList,
                    periodService.eventsInPeriod,
                    periodService.calendarTypes));
            }
            return $q.when(true);
        }

        function savePeriod() {
            return periodService.savePeriod(service.loadedPeriod).then(data => {
                service.loadedPeriod.changed(false);
                return data;
            });
        }

        function deletePeriod() {
            return periodService.deletePeriod(service.loadedPeriod);
        }

        function activateCurrentPeriod() {
            const periodBoundary = getCurrentPeriodBoundary();

            const foundedPeriod = periodService.knownPeriod(periodBoundary);
            if (foundedPeriod !== false) {
                return loadPeriodByHeader(foundedPeriod);
            }
            return $q.all([paramService.getParamList(),
                periodService.getPeriodGeneralInfo(periodBoundary),
                periodService.getCalendarTypes(),
                periodService.getUserEventsInPeriod(periodBoundary),
                userService.getUserList(),
                userParamService.getUserParamsList()
            ]).then(() => {
                service.loadedPeriod = new periodObjectFactory(periodBoundary,
                    periodService.periodGeneralInfo,
                    userService.userList,
                    paramService.paymentParams,
                    userParamService.userParamsList,
                    periodService.eventsInPeriod,
                    periodService.calendarTypes
                );
                return service.loadedPeriod
            });

        }

        function getCurrentPeriodBoundary() {
            let toDay = new Date();
            let periodEnd;
            let periodStart;
            toDay.setHours(0, 0, 0, 0);
            if (toDay.getDate() <= settingsIts.barierDate) {
                toDay.setUTCDate(-1);
                periodEnd = new Date(toDay);
                toDay.setUTCDate(0)
                periodStart = new Date(toDay);

            }
            else {
                toDay.setUTCMonth(toDay.getUTCMonth() + 1);
                toDay.setUTCDate(-1)
                periodEnd = new Date(toDay);
                toDay.setUTCDate(0)
                periodStart = new Date(toDay);

            }
            $log.debug((new Date()).toLocaleString('en-GB'), 'salaryService', 'loadPeriodByHeader', 'get period', {
                periodStart,
                periodEnd
            });
            return {
                periodStart,
                periodEnd
            }
        }

    }
})();