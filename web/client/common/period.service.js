(function () {
    'use strict';

    angular.module('itspayments')
        .service('periodService', periodService);
    periodService.$inject = ['$http', '$log'];

    function periodService($http, $log) {
        const service = this;
        let inProgress = false;

        service.periods = [];
        service.calendarTypes = [];
        service.eventsInPeriod = [];
        service.periodGeneralInfo = {};

        service.getPeriods = getPeriods;
        service.getPeriodGeneralInfo = getPeriodGeneralInfo;
        service.getCalendarTypes = getCalendarTypes;
        service.getUserEventsInPeriod = getUserEventsInPeriod;
        service.knownPeriod = knownPeriod;
        service.savePeriod = savePeriod;
        service.loadPeriod = loadPeriod;
        service.deletePeriod = deletePeriod;

        function toUTCDate(dateInObj) {
            const dateObj = new Date(dateInObj);
            return `${dateObj.getFullYear()}-${(dateObj.getMonth() + 1) < 10 ? 0 : ''}${dateObj.getMonth() + 1}-${(dateObj.getDate() + 1) < 10 ? 0 : ''}${dateObj.getDate()}T00:00:00.000Z`;
        }

        function loadPeriod(id) {
            return $http.post('periods/getPeriodFullData', {id})
                .then(data => {
                    const periodObj = JSON.parse(data.data.period.datajson);
                    periodObj.id = data.data.period.id;
                    $log.debug((new Date()).toLocaleString('en-GB'), 'periodService', 'loadPeriod', 'received', periodObj);
                    return periodObj;
                });
        }

        function deletePeriod(period) {
            return $http.post('periods/deletePeriod', {id: period.id})
        }

        function savePeriod(period) {
            const data = {
                id: period.id,
                periodStart: toUTCDate(period.periodStart),
                periodEnd: toUTCDate(period.periodEnd),
                periodObj: JSON.stringify(period)
            };
            let savePromice;
            if (parseInt(data.id) === 0) {
                savePromice = $http.post('periods/save', data).then(data => {
                    period.id = data.data.period.id
                });
            } else {
                savePromice = $http.put('periods/save', data);
            }

            return savePromice.then(() => getPeriods(true));
        }

        function isSameDate(date1, date2) {

        }

        function knownPeriod(periodBoundary) {
            if (!periodBoundary) {
                return false;
            }
            const foundPeriod = service.periods.filter(period => toUTCDate(periodBoundary.periodStart) === toUTCDate(period.periodStart)
                && toUTCDate(periodBoundary.periodEnd) === toUTCDate(period.periodEnd)).pop();
            return foundPeriod ? foundPeriod : false;

        }

        function getCalendarTypes() {
            return $http.post('periods/calendarTypes')
                .then(
                    (data) => {
                        service.calendarTypes.splice(0, service.calendarTypes.length);
                        data.data.map((val) => {
                            service.calendarTypes.push(val);
                        });
                        $log.debug((new Date()).toLocaleString('en-GB'), 'periodService', 'getCalendarTypes', 'data received', data);
                    }
                );
        }

        function getUserEventsInPeriod(period) {
            return $http.post('periods/getUserEvents',
                {periodStart: toUTCDate(period.periodStart), periodEnd: toUTCDate(period.periodEnd)})
                .then(
                    (data) => {
                        $log.debug((new Date()).toLocaleString('en-GB'), 'periodService', 'getUserEventsInPeriod', 'data received', data);
                        service.eventsInPeriod = data.data;
                        return data.data;
                    }
                );
        }

        function getPeriodGeneralInfo(period) {
            return $http.post('periods/getLength',
                {periodStart: period.periodStart, periodEnd: toUTCDate(period.periodEnd)})
                .then(
                    (data) => {
                        service.periodGeneralInfo = {};
                        service.periodGeneralInfo.workdays = data.data.workdays;
                        service.periodGeneralInfo.holidays = data.data.holidays;
                        service.periodGeneralInfo.totalDays = data.data.totalDays;
                        return service.periodGeneralInfo;
                    }
                );
        }

        function getPeriods(force) {
            if (!inProgress || force) {
                inProgress = $http.get('periods')
                    .then(function (data) {
                        service.periods.splice(0, service.periods.length);
                        if (Array.isArray(data.data)) {
                            data.data.map(
                                (periodEntry) => {
                                    service.periods.push(
                                        {
                                            id: parseInt(periodEntry.id),
                                            periodStart: new Date(`${periodEntry.periodStart} +0000`),
                                            periodEnd: new Date(`${periodEntry.periodEnd} +0000`)
                                        }
                                    );
                                });
                        }
                    });
            }
            return inProgress;
        }

        init();

        function init() {
            getPeriods();
        }
    }

})();