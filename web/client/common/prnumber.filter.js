(function () {
    'use strict';
    angular.module('itspayments')
        .filter('prnumber', function () {
            return function (input) {
                input = parseInt(input || '0', 10);
                //return input;
                if (input === 0) {
                    return 0;
                }
                let out = '.00';
                let sign = '';
                if (input < 0) {
                    sign = '-';
                    input *=-1;
                }
                for (let i = 0; input > 0; i++) {
                    out = `${input % 10}${out}`;
                    if ((i + 1) % 3 === 0) {
                        out = '\xa0' + out;
                    }
                    input = ~~(input / 10); // Math.floor()
                }

                return `${sign}${out}`;
            };
        })
})();