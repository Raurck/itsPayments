(function () {
    'use strict';
    angular.module('itspayments')
        .factory('periodObjectFactory', periodObjectFactory);

    periodObjectFactory.$inject = ['$log', 'settingsIts', 'calculationObjectFactory'];

    function periodObjectFactory($log, settingsIts, calculationObjectFactory) {

        function PeriodObject(periodBoundary, periodGeneralInfo, userList,
                              paymentParams, usersParamsList, periodEventList, eventTypes) {
            userList = userList || [];
            paymentParams = paymentParams || [];
            usersParamsList = usersParamsList || [];
            periodEventList = periodEventList || [];
            eventTypes = eventTypes || [];
            periodBoundary = periodBoundary || {};
            periodGeneralInfo = periodGeneralInfo || {};

            this.id = 0;

            this.periodStart = periodBoundary.periodStart;
            this.periodEnd = periodBoundary.periodEnd;

            this.info = {
                workdays: 0,
                holidays: 1,
                totalDays: 1,
                prepaymentDays: undefined,
                postpaymentDays: undefined,
                cardPaymentAmount: settingsIts.cardPaymentDefaultAmount,
                prepaidLock: false,
                postpaidLock: false,
                isLoaded: false,
                isSaved: false
            };

            setInfo(this, periodGeneralInfo);

            this.params = {
                eventTypes: angular.copy(eventTypes)
            };

            this.userList = this.fillUserList(userList, paymentParams, usersParamsList, periodEventList);
        }

        PeriodObject.prototype = {
            calculate: function () {
                if (!(this.postpaidLock && this.prepaidLock)) {
                    this.changed(true);
                }
                this.userList.map(user => user.calculationObject.calculate(this.info));
            },

            addBonus: function (user, name, amount) {
                if (!this.info.isPostpaymentLocked) {
                    this.changed(true);
                    user.calculationObject.addBonus(name, amount, this.info);
                }
            },

            getFullPostpaySum: function () {
                return this.userList.reduce((sum, user) =>
                    sum + saveParseFloat(user.calculationObject.postpaymentReal)
                    , 0);
            },

            getFullPrepaySum: function () {
                return this.userList.reduce((sum, user) =>
                    sum + saveParseFloat(user.calculationObject.prepaymentReal)
                    , 0);

            },

            getFullCardSum: function () {
                return this.userList.reduce((sum, user) =>
                    sum + saveParseFloat(user.calculationObject.cardPaymentAmount)
                    , 0);
            },

            getFullFOTSum: function () {
                return this.userList.reduce((sum, user) =>
                    sum + user.calculationObject.getUserFullSumReal()
                    , 0);
            },

            changed: function (state) {
                this.info.isSaved = !state;
            },

            removeBonus: function (user, bonus) {
                if (!this.info.isPostpaymentLocked) {
                    this.changed(true);
                    user.calculationObject.removeBonus(bonus, this.info);
                }
            },

            renewUserList: function (userList, paymentParams, usersParamsList, periodEventList, eventTypes) {
                if (this.info.isLoaded) {
                    this.userList.map(user => {
                        user.calculationObject.renewPeriodEvents(periodEventList[user.id] || []);
                    });

                    this.userList.map(user => {
                        user.calculationObject.renewUserSymbols(filterUsersParams(user.id, paymentParams, usersParamsList) || []);
                    });


                    if (!this.info.prepaidLock) {
                        this.params.eventTypes = angular.copy(eventTypes);
                    }
                    if (!this.info.postpaidLock) {
                        const tmpUser = this.fillUserList(userList, paymentParams, usersParamsList, periodEventList);
                        this.userList = this.userList.concat(tmpUser.filter(tmpUser => this.userList.filter(user => user.id === tmpUser.id).length === 0));

                        if (this.info.prepaidLock && eventTypes && Array.isArray(eventTypes) && eventTypes.length > 0) {
                            this.params.prepaidEventTypes = angular.copy(this.params.eventTypes);
                            this.params.eventTypes = angular.copy(eventTypes);
                        }
                    }
                }
                $log.debug((new Date()).toLocaleString('en-GB'), 'periodObjectFactory', 'renewUserList', 'new data', this);
            },

            fillUserList: function (userList, paymentParams, usersParamsList, periodEventList) {
                if (Array.isArray(userList)
                    && Array.isArray(usersParamsList)) {
                    return userList.filter(user =>
                        parseInt(user.state) === 1
                        && Array.isArray(usersParamsList[parseInt(user.id)])
                        && usersParamsList[parseInt(user.id)]
                            .reduce((hasAnyParam, currentParam) => hasAnyParam || currentParam, false))
                        .map(user => {
                            return {
                                id: user.id,
                                fullname: user.fullname,
                                calculationObject: new calculationObjectFactory(
                                    user.id,
                                    filterUsersParams(user.id, paymentParams, usersParamsList),
                                    periodEventList[user.id] || [])
                            }
                        });
                }
                return [];
            },

            getCalendarTypeById: function (typeId) {
                return this.params.eventTypes.filter(x => x.id === typeId).pop() || {};
            },

            getUserCardPayment:function(user){
                    return user.getUserCardPayment(this.info);
            },

            assign: function (dataObj) {
                if (!dataObj) {
                    $log.warn((new Date()).toLocaleString('en-GB'), 'periodObjectFactory', 'assign', 'dataObj was empty');
                    return;
                }
                Object.assign(this, dataObj);
                /*this.id = dataObj.id;
                //this.params = angular.copy(dataObj.params);
                //this.periodStart = dataObj.periodStart;
                //this.periodEnd = dataObj.periodEnd;
                //setInfo(this, dataObj.info);
                //this.userList = angular.copy(dataObj.userList);*/
                this.info.isLoaded = true;

                const tmp = (new calculationObjectFactory());
                this.userList.map(userObj => {
                    let x = Object.create(tmp.__proto__);
                    Object.assign(x, userObj.calculationObject);
                    userObj.calculationObject = x;
                });

                this.changed(false);
                $log.debug((new Date()).toLocaleString('en-GB'), 'periodObjectFactory', 'assign', 'dataObj assign result', this);
            }
        };

        function setInfo(periodObj, newInfo) {
            periodObj.info.workdays = newInfo.workdays || periodObj.info.workdays;
            periodObj.info.holidays = newInfo.holidays || periodObj.info.holidays;
            periodObj.info.totalDays = newInfo.totalDays || periodObj.info.totalDays;
            periodObj.info.prepaymentDays = newInfo.prepaymentDays || periodObj.info.prepaymentDays;
            periodObj.info.postpaymentDays = newInfo.postpaymentDays || periodObj.info.postpaymentDays;
            periodObj.info.cardPaymentAmount = newInfo.cardPaymentAmount || periodObj.info.cardPaymentAmount;
            periodObj.info.isPrepaymentLocked = newInfo.isPrepaymentLocked || periodObj.info.isPrepaymentLocked;
            periodObj.info.isPostpaymentLocked = newInfo.isPostpaymentLocked || periodObj.info.isPostpaymentLocked;
        }

        function filterUsersParams(userId, paymentParams, usersParamsList) {
            $log.debug((new Date()).toLocaleString('en-GB'), 'periodService', 'filterUsersParams', ' with', arguments, 'start');
            if (Array.isArray(paymentParams)
                && Array.isArray(usersParamsList)
                && Array.isArray(usersParamsList[parseInt(userId)])) {
                return paymentParams
                    .filter(param =>
                        usersParamsList[parseInt(userId)]
                            .filter((currentParamEnabled, idx) => parseInt(idx) === parseInt(param.id)
                                && currentParamEnabled)
                            .length > 0);
            }
            return [];
        }

        function saveParseFloat(Number) {
            const result = parseFloat(Number);
            return isNaN(result) ? 0 : result;
        }


        return PeriodObject;
    }
})
();