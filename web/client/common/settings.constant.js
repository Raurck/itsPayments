(function () {
    'use strict';
    angular.module('itspayments')
        .constant('settingsIts', {
            cardPaymentDefaultAmount: 7400,
            cardPaymentIfNoSymbolId: 6,
            barierDate:18
        });
})();