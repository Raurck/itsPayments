(function () {
    'use strict';
    angular.module('itspayments')
        .service('userParamService', userParamService);

    userParamService.$inject = ['$http', '$log'];

    function userParamService($http, $log) {
        const service = this;
        service.userParamsList = [];
        service.getUserParamsList = getUserParamsList;
        service.saveUserParamsList = saveUserParamsList;
        let inProgress = false;

        init();

        function init() {
            getUserParamsList();
        }

        function getUserParamsList(force) {
            if (!inProgress || force) {
                inProgress = $http.get('userparams').then(function (data) {
                    service.userParamsList.splice(0, service.userParamsList.length);
                    data.data.map(function (element) {
                        service.userParamsList[element.person_id] = service.userParamsList[element.person_id] || [];
                        service.userParamsList[element.person_id][element.param_id] = parseInt(element.active);
                    });
                    $log.debug(service.userParamsList);
                    return data.data;
                });
            }
            return inProgress;
        }

        function saveUserParamsList() {
            return $http.post('userparams/saveall', {params: service.userParamsList})
                .then(()=>getUserParamsList(true));
        }

    }
})();