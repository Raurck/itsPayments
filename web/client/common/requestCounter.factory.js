(function() {
    'use strict';
    angular.module('itspayments')
        .factory('requestCounter', requestCounter);

    requestCounter.$inject = ['$q','$log'];

    function requestCounter($q, $log) {
        let requests = 0;

        function request(config) {
            requests++;
            $log.debug(requests);
            return $q.when(config);
        }


        function requestError(error) {
            requests--;
            $log.debug(requests);
            return $q.reject(error);
        }


        function response(responseInt) {
            requests--;
            $log.debug(requests);
            return $q.when(responseInt);
        }


        function responseError(error) {
            requests--;
            $log.debug(requests);
            return $q.reject(error);
        }
        function getRequestCount() {
            return requests;
        }

        return {
            request: request,
            response: response,
            requestError: requestError,
            responseError: responseError,
            getRequestCount: getRequestCount
        }
    }
})();