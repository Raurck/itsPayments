(function () {
    angular.module('itspayments')
        .service('userService', userService);

    userService.$inject = ['$http'];

    function userService($http) {
        const service = this;
        service.userList = [];
        service.getUserList = getUserList;
        let inProgress = false;

        init();

        function init() {
            getUserList();
        }

        function getUserList(force) {
            if (!inProgress || force) {
                inProgress = $http.get('users').then(function (data) {
                    service.userList.splice(0, service.userList.length);
                    data.data.map(function (element) {
                        service.userList.push(element);
                    });
                    return data.data;
                });
            }
            return inProgress;
        }

    }
})();