(function () {
    angular.module('itspayments')
        .service('paramService', paramService);

    paramService.$inject = ['$http'];

    function paramService($http) {
        const service = this;
        let inProgress = false;
        service.getParamList = getParamList;
        service.saveParam = saveParam;
        service.paymentParams = [];

        init();

        function init() {
            getParamList();
        }


        function saveParam(param) {
            let promise;
            if (param.id) {
                promise = $http.put('paymentsparams/save', {param});
            } else {
                promise = $http.post('paymentsparams/save', {param});
            }
            return promise.then((data) => {
                const result = data.data.result;
                const tmpArr = service.paymentParams.filter(params => params.id === result.id)
                if (tmpArr.length > 0) {
                    let tmp = tmpArr.pop();
                    tmp.param = result.param;
                    tmp.relatedField = result.relatedField;
                    tmp.monthlySalary = result.monthlySalary;
                    tmp.active = result.active;
                    tmp.icon = result.icon;
                    tmp.class = result.class;
                }
                else {
                    service.paymentParams.push(result);
                }
                return result;
            });

        }

        function getParamList(force) {
            if (!inProgress || force) {
                inProgress = $http.get('paymentsparams').then((data) => {
                    service.paymentParams.splice(0, service.paymentParams.length);
                    data.data.map((element) => {
                        service.paymentParams.push(element);
                    });
                    return data.data;
                });
            }
            return inProgress;
        }

    }
})();