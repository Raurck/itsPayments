(function () {
    'use strict';
    angular.module('itspayments')
        .factory('calculationObjectFactory', calculationObjectFactory);

    calculationObjectFactory.$inject = ['settingsIts'];

    function calculationObjectFactory(settingsIts) {

        function CalculationObject(userId, symbolList, extendedParam) {
            extendedParam = extendedParam || {};
            symbolList = symbolList || [];
            this.userId = userId;
            this.symbols = {
                prepaymentCalc: [],
                postpaymentCalc: [],
                symbolList: angular.copy(symbolList.filter(param => param.active)),
                incidentSymbolList: symbolList.filter(param => !!param.relatedField),
            };
            this.bonuses = [];
            this.extendedParams = angular.copy(extendedParam);
            this.prepaymentCalc = 0;
            this.postpaymentCalc = 0;
            this.prepaymentReal = 0;
            this.postpaymentReal = 0;
            this.cardPaymentAmount = 0;
            this.cardPaymentAmountPlaned = 0;
            this.prepaymentDays = null;
            this.postpaymentDays = null;
        }

        CalculationObject.prototype = {
            addBonus: function (name, amount, paymentInfo) {
                if (!name || isNaN(amount)) {
                    return false;
                }
                this.bonuses.push({name, amount});
                this.calculatePostpaymentSum(paymentInfo);
            },

            removeBonus: function (bonus, paymentInfo) {
                const remIndex = this.bonuses.indexOf(bonus);
                if (remIndex >= 0) {
                    this.bonuses.splice(remIndex, 1);
                }
                this.calculatePostpaymentSum(paymentInfo);
            },

            renewPeriodEvents: function (extendedParam) {
                this.extendedParams = angular.copy(extendedParam);
            },

            renewUserSymbols: function (symbolsList) {
                this.symbols.symbolList = angular.copy(symbolsList);
                this.symbols.incidentSymbolListFiltred = undefined;
                this.symbols.symbolListFiltred = undefined;
            },

            calculatePostpaymentSum: function (paymentInfo) {
                const symbolSum = calculatePostpaymentSymbolSum(this);
                const bonusSum = calculateBonusSum(this);
                const cardSum = calculateCardSum(this, paymentInfo);
                this.cardPaymentAmount = cardSum;
                this.postpaymentCalc = (symbolSum + bonusSum - cardSum);
                this.postpaymentReal = this.postpaymentCalc.toFixed(2);
            },

            calculatePrepaymentSum: function () {
                const symbolSum = calculatePrepaymentSymbolSum(this);
                this.prepaymentCalc = symbolSum;
                this.prepaymentReal = this.prepaymentCalc.toFixed(2);
            },

            calculate: function (periodInfo) {
                this.symbols.incidentSymbolListFiltred = undefined;
                this.symbols.symbolListFiltred = undefined;
                const workDays = periodInfo.workdays;

                if (!periodInfo.prepaidLock) {
                    const prepaymentDays = getUserPrepaidDaysCount(this, periodInfo);
                    const prepayPart = (workDays && workDays >= 0) ? prepaymentDays / workDays : 0;

                    this.symbols.prepaymentCalc.splice(0, this.symbols.prepaymentCalc.length);
                    this.symbols.symbolList.map(param => {
                        let monthlySalary = parseFloat(param.monthlySalary);
                        monthlySalary = isNaN(monthlySalary) ? 0 : monthlySalary;

                        this.symbols.prepaymentCalc
                            .push({
                                id: parseInt(param.id),
                                amount: ((!param.relatedField) ? prepayPart : 0) * monthlySalary
                            });

                    });
                    this.calculatePrepaymentSum(periodInfo);
                }

                if (!periodInfo.postpaidLock) {
                    const postpaymentDays = getUserPostpaidDaysCount(this, periodInfo);
                    const postpayPart = (workDays && workDays >= 0) ? postpaymentDays / workDays : 0;

                    this.symbols.postpaymentCalc.splice(0, this.symbols.postpaymentCalc.length);
                    this.symbols.symbolList.map(param => {
                        let monthlySalary = parseFloat(param.monthlySalary);
                        monthlySalary = isNaN(monthlySalary) ? 0 : monthlySalary;

                        this.symbols.postpaymentCalc
                            .push({
                                id: parseInt(param.id),
                                amount: ((!param.relatedField) ? postpayPart : this.getExtendedParamValue(param.relatedField)) * monthlySalary
                            });

                    });
                    this.calculatePostpaymentSum(periodInfo);
                }
            },

            getExtendedParamValue: function (paramName) {
                if (!(this.extendedParams
                        && this.extendedParams[paramName])) {
                    return 0;
                }
                const tmpRes = parseInt(this.extendedParams[paramName]);
                return isNaN(tmpRes) ? 0 : tmpRes;
            },

            haveCalendarEvents: function (paramName) {
                return (this.extendedParams
                    && this.extendedParams.events);
            },

            symbolsListFilterd: function () {
                if (!this.symbols.symbolListFiltred) {
                    this.symbols.symbolListFiltred = this.symbols.symbolList.filter(param =>
                        (this.getPrepaidBySymbol(param.id) + this.getPostpaidBySymbol(param.id)) > 0
                    );
                }
                return this.symbols.symbolListFiltred;
            },

            incidentSymbolListFiltered: function () {
                if (!this.symbols.incidentSymbolListFiltred) {
                    this.symbols.incidentSymbolListFiltred = this.symbols.incidentSymbolList.filter((param) => this.extendedParams[param.relatedField] > 0);
                }
                return this.symbols.incidentSymbolListFiltred;
            },

            haveCountedParams: function () {
                return this.symbols.incidentSymbolList.reduce((count, param) => count + this.extendedParams[param.relatedField], 0) > 0;
            },

            getPrepaidBySymbol: function (paramId) {
                const res = this.symbols.prepaymentCalc.filter(calcRow => calcRow.id === paramId).pop();
                if (res) {
                    return res.amount;
                }
                return 0;
            },

            getPostpaidBySymbol: function (paramId) {
                const res = this.symbols.postpaymentCalc.filter(calcRow => calcRow.id === paramId).pop();
                if (res) {
                    return res.amount;
                }
                return 0;
            },

            getUserFullSumCalc: function () {
                return (this.prepaymentCalc + this.postpaymentCalc + parseFloat(this.cardPaymentAmount));
            },

            getUserFullSumReal: function () {
                return (saveParseFloat(this.prepaymentReal)
                    + saveParseFloat(this.postpaymentReal)
                    + saveParseFloat(this.cardPaymentAmount));
            },

            getUserCardPayment: function (paymentInfo) {
                return (calculateCardSum(this, paymentInfo));
            },

            getDelta: function () {
                return this.getUserFullSumReal() - this.getUserFullSumCalc();
            },

            cardPaymentRequired: function () {
                return this.symbols.postpaymentCalc.filter(calcRow => calcRow.id === settingsIts.cardPaymentIfNoSymbolId).length === 0
            }

        };

        function saveParseFloat(Number) {
            const result = parseFloat(Number);
            return isNaN(result) ? 0 : result;
        }

        function calculatePostpaymentSymbolSum(userCalcObject) {
            return userCalcObject.symbols.postpaymentCalc.reduce((sum, value) => sum + value.amount, 0);
        }

        function calculatePrepaymentSymbolSum(userCalcObject) {
            return userCalcObject.symbols.prepaymentCalc.reduce((sum, value) => sum + value.amount, 0);
        }

        function calculateBonusSum(userCalcObject) {
            return userCalcObject.bonuses.reduce((sum, val) => sum + val.amount, 0);
        }

        function calculateCardSum(userCalcObject, paymentInfo) {
            if (userCalcObject.cardPaymentRequired()) {
                let amount = 0;
                //if (paymentInfo && paymentInfo.cardPaymentAmount) {
                amount = parseFloat(userCalcObject.cardPaymentAmountPlaned || (paymentInfo && paymentInfo.cardPaymentAmount ? paymentInfo.cardPaymentAmount : 0) || 0);
                //}
                //userCalcObject.cardPaymentAmount = parseFloat(userCalcObject.cardPaymentAmount || amount);
                return amount;//userCalcObject.cardPaymentAmount;
            }
            return 0;
        }

        function getUserPrepaidDaysCount(userCalcObject, periodInfo) {
            if (userCalcObject.prepaymentDays) {
                return userCalcObject.prepaymentDays;
            }

            if (userCalcObject.postpaymentDays) {
                return periodInfo.workdays - userCalcObject.postpaymentDays;
            }

            if (periodInfo.prepaymentDays) {
                return periodInfo.prepaymentDays;
            }

            if (periodInfo.postpaymentDays) {
                return periodInfo.workdays - periodInfo.postpaymentDays;
            }

            return Math.floor(parseFloat(periodInfo.workdays) / 2);
        }

        function getUserPostpaidDaysCount(userCalcObject, periodInfo) {
            if (userCalcObject.postpaymentDays) {
                return userCalcObject.postpaymentDays;
            }

            if (userCalcObject.prepaymentDays) {
                return periodInfo.workdays - userCalcObject.prepaymentDays;
            }

            if (periodInfo.postpaymentDays) {
                return periodInfo.postpaymentDays;
            }

            if (periodInfo.prepaymentDays) {
                return periodInfo.workdays - periodInfo.prepaymentDays;
            }

            return Math.ceil(parseFloat(periodInfo.workdays) / 2);
        }

        return CalculationObject;
    }
})
();