<?php
/**
 * Created by PhpStorm.
 * User: agkufko
 * Date: 08.08.17
 * Time: 16:42
 */
return [
    [
        'id' => 1,
        'param' => "Стажер",
        'relatedField' => null,
        'montlySalary' => 13650
    ],
    [
        'id' => 2,
        'param' => "Инженер",
        'relatedField' => null,
        'montlySalary' => 22750
    ],
    [
        'id' => 3,
        'param' => "Категория 2",
        'relatedField' => null,
        'montlySalary' => 7000
    ],
    [
        'id' => 4,
        'param' => "Категория 1",
        'relatedField' => null,
        'montlySalary' => 20000
    ],
    [
        'id' => 5,
        'param' => "Водитель",
        'relatedField' => null,
        'montlySalary' => 3000
    ],
    [
        'id' => 6,
        'param' => "Без ТУ",
        'relatedField' => null,
        'montlySalary' => 3000
    ],
    [
        'id' => 7,
        'param' => "Персональное закрепление",
        'relatedField' => null,
        'montlySalary' => 5000
    ],
    [
        'id' => 8,
        'param' => "Дежурства вечерние",
        'relatedField' => "periodDutyCounts",
        'montlySalary' => 700
    ],
    [
        'id' => 9,
        'param' => "Дежурства в выходные дни",
        'relatedField' => "periodHolyDayDutyCounts",
        'montlySalary' => 2000
    ],
];

