CREATE TABLE employee_salary_journal(
  id SERIAL NOT NULL PRIMARY KEY,
  person_id INT NOT NULL,
  periodStart DATETIME,
  periodEnd DATETIME,
  periodDutyCounts INT,
  periodHolyDayDutyCounts INT,

  FOREIGN KEY (person_id)
    REFERENCES users(id)
);


-- statement

CREATE TABLE  employee_salary_params(
  id SERIAL NOT NULL PRIMARY KEY,
  param character varying(255) NOT NULL,
  relatedField character varying(255),
  monthlySalary DECIMAL,
  class character varying(255),
  icon  character varying(255)
);

-- statement

CREATE TABLE  employee_bonuses(
  id SERIAL NOT NULL PRIMARY KEY,
  payment_id INT NOT NULL,
  amount DECIMAL NOT NULL,
  descr character varying(255) NOT NULL,

  FOREIGN KEY (payment_id)
  REFERENCES employee_salary_journal(id)
);

-- statement

CREATE TABLE  employee_active_params(
  id SERIAL PRIMARY KEY,
  param_id int NOT NULL,
  person_id int NOT NULL,

  FOREIGN KEY (param_id)
    REFERENCES employee_salary_params(id),
  FOREIGN KEY (person_id)
    REFERENCES users(id)
);
