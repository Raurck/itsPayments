CREATE TABLE `hd`.`salary_periods` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `periodStart` DATETIME NOT NULL,
  `periodEnd` DATETIME NOT NULL,
  PRIMARY KEY (`id`));