ALTER TABLE `hd`.`employee_salary_journal`
DROP COLUMN `periodEnd`,
DROP COLUMN `periodStart`,
ADD COLUMN `period_id` INT NOT NULL AFTER `periodHolyDayDutyCounts`,
ADD INDEX `fk_employee_salary_journal_1_idx` (`period_id` ASC);
ALTER TABLE `hd`.`employee_salary_journal`
ADD CONSTRAINT `fk_employee_salary_journal_1`
  FOREIGN KEY (`period_id`)
  REFERENCES `hd`.`salary_periods` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
