angular.module('itsPay', [], function() {});

angular.module('itsPay')
    .filter('prnumber', function() {
        return function(input) {
            input = parseInt(input || '0', 10);
            //return input;
            if (input === 0) {
                return 0;
            }
            let out = '.00';
            for (let i = 0; input > 0; i++) {
                out = `${input % 10}${out}`;
                if ((i + 1) % 3 === 0) {
                    out = '\xa0' + out;
                }
                input = ~~(input / 10); // Math.floor()
            }

            return out;
        };
    })
    .service('personsInfo', ['$http', function($http) {
        const personsInfoService = this;
        personsInfoService.persons = [];
        personsInfoService.date = {
            month: 'Месяц',
            year: 'Год'
        };
        personsInfoService.getPersonsData = getPersonsData;
        activate();

        function activate() {
            personsInfoService.getPersonsData();
        }

        function getPersonsData() {
            $http.get('js/person.json').then(
                function(response) {
                    personsInfoService.persons.splice(0, personsInfoService.persons.length);
                    response.data.persons.forEach(function(currentValue) { personsInfoService.persons.push(currentValue); });
                    personsInfoService.date.month = response.data.month;
                    personsInfoService.date.year = response.data.year;
                    return response.data;
                }
            );
        }
    }])
    .controller('billingsCtrl', ['$scope', 'personsInfo', function($scope, personsInfo) {
        $scope.persons = personsInfo.persons;
        $scope.date = personsInfo.date;
    }]);
